package TestBase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import Helper.WaitHelper;
import io.github.bonigarcia.wdm.WebDriverManager;

public class eTestBase {
	public static WebDriverWait wait;
	public SoftAssert sA;
	public static WebDriver driver;
	public static Properties prop;
	public static Logger log = Logger.getLogger(eTestBase.class);
	public static String times;

	public eTestBase() {
		times = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		sA = new SoftAssert();
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + 
					"/src/main/resources/Files/Config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void initialization(String siteUrl) {
		String BrowserName = prop.getProperty("Browser");
		if (prop.getProperty("Browser").equalsIgnoreCase(BrowserName)) {
//		System.setProperty("webdriver.chrome.driver",
//					System.getProperty("user.dir") + "\\src\\test" + "\\Executeable\\chromedriver.exe");
			WebDriverManager.chromedriver().setup();
     		ChromeOptions option = new ChromeOptions();
     		
     		option.addArguments("disable-features=NetworkService"); 
     		
			driver = new ChromeDriver(option);
		}
		wait = new WebDriverWait(driver, 80);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(WaitHelper.page_load, TimeUnit.SECONDS);
		driver.get(siteUrl);
		// driver.get(ExcelReadTestData.Url);
	}

	public static void Browserquit() {
		if (driver != null) {
			driver.quit();
		}
	}
}
