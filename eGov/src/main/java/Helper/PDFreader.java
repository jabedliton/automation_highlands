package Helper;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import TestBase.eTestBase;

public class PDFreader extends eTestBase{
public String docUrl;
	public void ReadingPDF(String Verification) throws IOException  {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		docUrl = driver.getCurrentUrl();
		log.info("PDF Url:" + docUrl);
		URL url=new URL(docUrl);
		InputStream in=url.openStream();
		BufferedInputStream is=new BufferedInputStream(in);
		PDDocument document=null;
		document=PDDocument.load(is);
		String documentBody=new PDFTextStripper().getText(document);
		document.close();
		if(documentBody.contains(Verification)) {
		log.info("Document contain  ="+ Verification);
		log.info("PDF Url:" + docUrl);
			
		}else {
			log.info("Document do not contain  ="+ Verification);
		}
		
	}
	
	
	
}
