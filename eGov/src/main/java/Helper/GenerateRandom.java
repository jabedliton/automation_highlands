package Helper;

import org.testng.annotations.Test;

import TestBase.eTestBase;

public class GenerateRandom extends eTestBase {
	@Test
    public int  R_num( ) {
      int min = 0;
      int max = 2;
      int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
      log.info("index number generated for bill by random class is ="+random_int);
	return random_int;
    }
}
