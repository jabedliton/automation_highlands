package Helper;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import TestBase.eTestBase;

public class SearchBill_Nocert extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	public Actions actions;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = "(//li[contains(.,'2020')])[2]")
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//div[@class='ng-modal show ng-isolate-scope']//table[@class='ng-scope']")
	public WebElement OP_Pop_Up;
	@FindBy(xpath = "//table[@class='ng-scope']//tbody//tr//td//input[@value='OK']")
	public WebElement Ok_Button;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//label[contains(.,'Bill Type')]")
	public WebElement billtype;
	@FindBy(xpath = "//input[contains(@class,'select2-search__field')]")
	public WebElement searchFields;
	@FindBy(xpath = "//span[contains(@id,'select2-status_flag-container')]")
	public WebElement flag;
	@FindBy(xpath = "//span[contains(@title,'NA-DO NOT ADVERTISE')]")
	public WebElement DonotAdvertise;

	public SearchBill_Nocert() {

		PageFactory.initElements(driver, this);
	}

	public void searchingBill(String SearchYear, String OperatorSign_Forbalance, String BillDueAmount, int Billindex) {

		driver.navigate().refresh();
		actions = new Actions(driver);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		wait.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
		EnterTaxYear.click();
		FilterTaxYear.sendKeys(SearchYear);
		String s="(//li[contains(.,"+"'"+SearchYear+"'"+")])"+"[4]";
	    log.info(s);
		WebElement e=driver.findElement(By.xpath(s));
		e.click();
		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
		BalanceAmount.sendKeys(BillDueAmount);
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", ClickOnSearch);
//		flag.click();
//		searchFields.sendKeys("NA-DO NOT ADVERTISE");
		driver.findElement(By.xpath("//li[contains(.,'NA-DO NOT ADVERTISE')]")).click();
		ClickOnSearch.click();
		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
		// @SuppressWarnings("unlikely-arg-type")
		@SuppressWarnings("unlikely-arg-type")
		String Bill_Amount = col.get(14).getText();
		// Float Billtype=Float.valueOf(col.get(2).getText());
		col.get(Billindex).click();
	}

}
