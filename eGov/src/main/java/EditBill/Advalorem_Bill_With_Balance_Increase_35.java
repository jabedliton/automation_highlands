package EditBill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.relevantcodes.extentreports.model.Log;

import TestBase.eTestBase;

public class Advalorem_Bill_With_Balance_Increase_35 extends eTestBase {
	public JavascriptExecutor js;
	public List<WebElement> ele;
	public WebElement we1;
	public List<WebElement> table;
	public ArrayList<String> myarr;
	public WebElement cc;
	@FindBy(xpath = "//input[contains(@id,'edit_bill_button')]")
	public WebElement editbill;
	@FindBy(xpath = "(//a[@href='#'][contains(.,'Ad-Valorem')])[1]")
	public WebElement adv;
	@FindBy(xpath = "//*[@id=\"advalorem-fl\"]/table/tbody[1]/tr/th[2]/input")
	public WebElement ckb;
	@FindBy(xpath = "//a[normalize-space()='Ad-Valorem']")
	public WebElement adv2;
	// a[normalize-space()='Ad-Valorem']
	@FindBy(xpath = "//a[@href='#'][contains(.,'Non Ad-Valorem Info')]")
	public WebElement nonad;
	@FindBy(xpath = "//textarea[contains(@ng-model,'bill_values.note')]")
	public WebElement note;
	@FindBy(xpath = "//input[contains(@ng-model,'bill_values.correction_number')]")
	public WebElement correctionNumber;
	@FindBy(xpath = "(//input[contains(@value,'Save Changes (F10)')])[2]")
	public WebElement Save;
	@FindBy(xpath = "//h2[contains(.,'Success')]")
	public WebElement messageCheck;
	@FindBy(xpath = "//button[@class='confirm'][contains(.,'OK')]")
	public WebElement OkButton;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Combined Info')]")
	public WebElement CombinedInfotab;
	@FindBy(xpath = "//legend[contains(.,'Ad Valorem Taxes')]")
	public WebElement AdValoremTaxesUnderCombileInfo;

	public Advalorem_Bill_With_Balance_Increase_35() {

		PageFactory.initElements(driver, this);
	}
	public void EditBillwithBalnce() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(editbill));
		editbill.click();
		wait.until(ExpectedConditions.visibilityOf(adv));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", adv);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(nonad));
			nonad.click();
			;
			adv2.click();
			driver.findElement(By.xpath("(//input[contains(@ng-disabled,'permissions.ppt_can_edit_corrections')])[3]"))
					.click();
		} catch (Exception e) {
			// TODO: handle exception
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[contains(@ng-model,'edit_levy_checkbox')]")).click();
		}
		WebElement e = driver.findElement(By.xpath("(//input[contains(@ng-model,'listing.valorem.just_value')])[1]"));
		wait.until(ExpectedConditions.elementToBeClickable(e));
		e.sendKeys("1");
		// @@clicking on else where for continue button
		driver.findElement(By.xpath("(//td[@colspan='4'])[2]")).click();
		// @@Clicking on continue
		WebElement ee = driver.findElement(By.cssSelector("input[ng-click='closeDown(allNoAdvalorem)']"));
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ee));
			ee.click();
		} catch (Exception e2) {
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("input[ng-click='closeDown(allNoAdvalorem)']")).click();
		}
		table = driver.findElements(By.xpath("//*[@id=\"ad_valorem_table\"]/tr"));
		myarr = new ArrayList<>();
		int row = 1;
		for (row = 1; row < table.size() + 1; row++) {
			String we = "(//input[@ng-model='listing.valorem.amount_levied'])";
			String elementString = we + "[" + row + "]";
			we1 = driver.findElement(By.xpath(elementString));
			log.info("Tax levied  amounts Amount is = " + we1.getAttribute("value").replaceAll("[$,]", ""));
			int i = row - 1;
			myarr.add(i, we1.getAttribute("value").replaceAll("[$,]", ""));
		}
		log.info(Arrays.toString(myarr.toArray()));
		js.executeScript("arguments[0].scrollIntoView();", note);
		note.sendKeys("Test");
		wait.until(ExpectedConditions.elementToBeClickable(correctionNumber));
		correctionNumber.sendKeys("01");
		Save.click();
	}
}
