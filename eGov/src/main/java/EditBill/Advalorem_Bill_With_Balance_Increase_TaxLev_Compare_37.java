package EditBill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.tools.ant.types.resources.comparators.Reverse;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.relevantcodes.extentreports.model.Log;

import TestBase.eTestBase;

public class Advalorem_Bill_With_Balance_Increase_TaxLev_Compare_37 extends eTestBase {
	public JavascriptExecutor js;
	public List<WebElement> ele;
	public WebElement we1;
	public String TaxLev ;
	public String TaxLevCom ;
	public List<WebElement> table;
	public ArrayList<String> myarr;
	public ArrayList<String> myComarr;
	public ArrayList<String> myComarr2;
	public WebElement cc;
	public WebElement webele;
	public List<WebElement> tab;
	@FindBy(xpath = "//input[contains(@id,'edit_bill_button')]")
	public WebElement editbill;
	@FindBy(xpath = "(//a[@href='#'][contains(.,'Ad-Valorem')])[1]")
	public WebElement adv;
	@FindBy(xpath = "//*[@id=\"advalorem-fl\"]/table/tbody[1]/tr/th[2]/input")
	public WebElement ckb;
	@FindBy(xpath = "//a[normalize-space()='Ad-Valorem']")
	public WebElement adv2;
	@FindBy(xpath = "//*[@id=\"tax_account_id\"]")
	public WebElement AccountNumber;
	
	@FindBy(xpath = "//a[@href='#'][contains(.,'Non Ad-Valorem Info')]")
	public WebElement nonad;
	@FindBy(xpath = "//textarea[contains(@ng-model,'bill_values.note')]")
	public WebElement note;
	@FindBy(xpath = "//input[contains(@ng-model,'bill_values.correction_number')]")
	public WebElement correctionNumber;
	@FindBy(xpath = "//*[@id=\"editBillFlContainer\"]/div[1]/input[2]")
	public WebElement Save;
	@FindBy(xpath = "//h2[contains(.,'Success')]")
	public WebElement messageCheck;
	@FindBy(xpath = "//button[@class='confirm'][contains(.,'OK')]")
	public WebElement OkButton;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Combined Info')]")
	public WebElement CombinedInfotab;
	@FindBy(xpath = "//legend[contains(.,'Ad Valorem Taxes')]")
	public WebElement AdValoremTaxesUnderCombileInfo;

	public Advalorem_Bill_With_Balance_Increase_TaxLev_Compare_37() {

		PageFactory.initElements(driver, this);
	}
	public void EditBillwithBalnce() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(editbill));
		editbill.click();
		wait.until(ExpectedConditions.visibilityOf(adv));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", adv);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(nonad));
			nonad.click();
			;
			adv2.click();
			driver.findElement(By.xpath("(//input[contains(@ng-disabled,'permissions.ppt_can_edit_corrections')])[3]"))
					.click();
		} catch (Exception e) {
			// TODO: handle exception
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[contains(@ng-model,'edit_levy_checkbox')]")).click();
		}
		WebElement e = driver.findElement(By.xpath("(//input[contains(@ng-model,'listing.valorem.just_value')])[1]"));
		wait.until(ExpectedConditions.elementToBeClickable(e));
		e.sendKeys("1");
		// @@clicking on else where for continue button
		driver.findElement(By.xpath("(//td[@colspan='4'])[2]")).click();
		// @@Clicking on continue
		WebElement ee = driver.findElement(By.cssSelector("input[ng-click='closeDown(allNoAdvalorem)']"));
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ee));
			ee.click();
		} catch (Exception e2) {
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("input[ng-click='closeDown(allNoAdvalorem)']")).click();
		}
		table = driver.findElements(By.xpath("//*[@id=\"ad_valorem_table\"]/tr"));
		myarr = new ArrayList<>();
		int row = 1;
		for (row = 1; row < table.size() + 1; row++) {
			String we = "(//input[@ng-model='listing.valorem.amount_levied'])";
			String elementString = we + "[" + row + "]";
			we1 = driver.findElement(By.xpath(elementString));
			//log.info("Tax levied  amounts Amount is = " + we1.getAttribute("value").replaceAll("[$,]", ""));
			int i = row - 1;
			myarr.add(i, we1.getAttribute("value").replaceAll("[$,]", ""));
		}
		log.info(Arrays.toString(myarr.toArray()));
		js.executeScript("arguments[0].scrollIntoView();", note);
		note.sendKeys("Test");
		wait.until(ExpectedConditions.elementToBeClickable(correctionNumber));
		correctionNumber.sendKeys("01");
		WebElement e1=driver.findElement(By.xpath("//div[13]/div[7]/div/button"));
		wait.until(ExpectedConditions.visibilityOf(e1));
		e1.click();
		Save.click();
		wait.until(ExpectedConditions.visibilityOf(CombinedInfotab));
		log.info("Account number ="+AccountNumber.getAttribute("value"));
		js.executeScript("arguments[0].scrollIntoView();",AdValoremTaxesUnderCombileInfo);
		tab = driver.findElements(By.xpath("//*[@id=\"adValoremTax\"]/table/tbody/tr"));
		myComarr=new ArrayList<>();
		int r = 1;
		int index=r-1;
		int col=7;
		
		for (r = 1; r < tab.size(); r++) {
				TaxLev = "//*[@id=\"adValoremTax\"]/table/tbody/tr[" + r + "]/td[" + col+ "]";
				cc= driver.findElement(By.xpath(TaxLev));
					
			myComarr.add(index,cc.getText().replaceAll("[$,]", ""));
				}
		Collections.reverse(myComarr);
		log.info(Arrays.toString(myComarr.toArray()));
		if(myarr.equals(myComarr)==true) {
			Assert.assertTrue(true);
			log.info("Arrays are matched");
			log.info("Edit Bill tax levied values as an Array ="+Arrays.toString(myarr.toArray()));
			log.info("Cominfo Bill tax levied values as an Array ="+Arrays.toString(myComarr.toArray()));
		}else {
			log.info("Arrays did not match Re running the Loop");
			myComarr2=new ArrayList<>();
			int rr = 1;
			int ind=rr-1;
			int column=8;
			for (rr = 1; rr < tab.size(); rr++) {
				TaxLevCom = "//*[@id=\"adValoremTax\"]/table/tbody/tr[" + rr + "]/td[" + column+ "]";
					webele= driver.findElement(By.xpath(TaxLevCom));
						
					myComarr2.add(ind,webele.getText().replaceAll("[$,]", ""));
					}
		
				Collections.reverse(myComarr2);
			log.info(Arrays.toString(myComarr2.toArray()));
			if(myarr.equals(myComarr2)==true) {
				
				Assert.assertTrue(true);
				log.info("Arrays are matched");
				log.info("Edit Bill tax levied values as an Array ="+Arrays.toString(myarr.toArray()));
				log.info("Cominfo Bill tax levied values as an Array ="+Arrays.toString(myComarr2.toArray()));
			}else {
				log.info("Arrays are not matched");
				log.info("Edit Bill tax levied values as an Array ="+Arrays.toString(myarr.toArray()));
				log.info("Cominfo Bill tax levied values as an Array ="+Arrays.toString(myComarr2.toArray()));
				Assert.fail("Arrays elements did not match");
			}
			 
			
		}
	
	}
	}
		
		
		
	

