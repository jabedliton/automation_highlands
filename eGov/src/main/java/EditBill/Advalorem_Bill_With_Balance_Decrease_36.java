package EditBill;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import TestBase.eTestBase;

public class Advalorem_Bill_With_Balance_Decrease_36 extends eTestBase {
	public JavascriptExecutor js;
	public List<WebElement> ele;
	public String ran;
	public String str;
	public String[] myarr;
	public WebElement cc;
	@FindBy(xpath = "//input[contains(@id,'edit_bill_button')]")
	public WebElement editbill;
	@FindBy(xpath = "(//a[@href='#'][contains(.,'Ad-Valorem')])[1]")
	public WebElement adv;
	@FindBy(xpath = "//*[@id=\"advalorem-fl\"]/table/tbody[1]/tr/th[2]/input")
	public WebElement ckb;
	@FindBy(xpath = "//a[normalize-space()='Ad-Valorem']")
	public WebElement adv2;
	// a[normalize-space()='Ad-Valorem']
	@FindBy(xpath = "//a[@href='#'][contains(.,'Non Ad-Valorem Info')]")
	public WebElement nonad;
	@FindBy(xpath = "//textarea[contains(@ng-model,'bill_values.note')]")
	public WebElement note;
	@FindBy(xpath = "//input[contains(@ng-model,'bill_values.correction_number')]")
	public WebElement correctionNumber;
	@FindBy(xpath = "(//input[contains(@value,'Save Changes (F10)')])[2]")
	public WebElement Save;
	@FindBy(xpath = "//h2[contains(.,'Success')]")
	public WebElement messageCheck;
	@FindBy(xpath = "//button[@class='confirm'][contains(.,'OK')]")
	public WebElement OkButton;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Combined Info')]")
	public WebElement CombinedInfotab;
	@FindBy(xpath = "(//div[contains(@class,'ng-modal-dialog-content')])[3]")
	public WebElement cont;
	@FindBy(xpath = "//legend[contains(.,'Ad Valorem Taxes')]")
	public WebElement AdValoremTaxesUnderCombileInfo;
	public Advalorem_Bill_With_Balance_Decrease_36() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "unlikely-arg-type", "unchecked" })
	public void EditBillwithBalnce() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(editbill));
		editbill.click();
		wait.until(ExpectedConditions.visibilityOf(adv));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", adv);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(nonad));
			nonad.click();
			adv2.click();
			driver.findElement(By.xpath("(//input[contains(@ng-disabled,'permissions.ppt_can_edit_corrections')])[3]"))
					.click();
		} catch (Exception e) {
			// TODO: handle exception
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[contains(@ng-model,'edit_levy_checkbox')]")).click();
		}
		WebElement e = driver.findElement(By.xpath("(//input[contains(@ng-model,'listing.valorem.just_value')])[1]"));
		String val=e.getAttribute("value");
		log.info("Value before decrease ="+val);
		e.clear();
		int value=Integer.valueOf(val);
		int NewValue=value-100;
		wait.until(ExpectedConditions.elementToBeClickable(e));
		e.sendKeys(String.valueOf(NewValue));
		log.info("New value after change ="+NewValue);
		// @@clicking on else where for continue button
		
		// @@Clicking on continue
		try {
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("(//td[@colspan='4'])[2]"))));
			driver.findElement(By.xpath("(//td[@colspan='4'])[2]")).click();
		} catch (Exception e2) {
			// TODO: handle exception
		}
		WebElement ee = driver.findElement(By.xpath("//input[@ng-click='closeDown(allNoAdvalorem)']"));
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ee));
			ee.click();
		} catch (Exception e2) {
			Thread.sleep(5000);
			driver.findElement(By.cssSelector("input[ng-click='closeDown(allNoAdvalorem)']")).click();
		}
		
		List<WebElement> table = driver.findElements(By.xpath("//*[@id=\"ad_valorem_table\"]/tr"));
		System.out.println(table.size());
	
		
		int row = 1;
		for (row = 1; row < table.size()+1; row++) {
			String we = "(//input[@ng-model='listing.valorem.amount_levied'])";
			String elementString = we + "[" + row + "]";
			WebElement we1 = driver.findElement(By.xpath(elementString));
			log.info("Tax levied  amounts Amount is = " + we1.getAttribute("value").replaceAll("[$,]", ""));
			
		}
		
		js.executeScript("arguments[0].scrollIntoView();", note);
		note.sendKeys("Test");
		wait.until(ExpectedConditions.elementToBeClickable(correctionNumber));
		correctionNumber.sendKeys("01");
		Save.click();
		wait.until(ExpectedConditions.visibilityOf(CombinedInfotab));
		js.executeScript("arguments[0].scrollIntoView();",AdValoremTaxesUnderCombileInfo);
		List<WebElement> tab = driver.findElements(By.xpath("//*[@id=\"adValoremTax\"]/table/tbody/tr"));
		
		
		int r = 1;
		int cl = 7;
		for (r = 1; r < tab.size(); r++) {
			String ab = "//*[@id=\"adValoremTax\"]/table/tbody/tr[" + r + "]/td[" + cl + "]";
			WebElement cc = driver.findElement(By.xpath(ab));
			log.info("Levied amount under Combine Info is=" + cc.getText().replaceAll("[$,]", ""));
				
		}
	}
}
