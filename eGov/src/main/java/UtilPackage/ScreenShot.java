package UtilPackage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import TestBase.eTestBase;

public class ScreenShot extends eTestBase {
	public File src_p, src_f;
	// String D=System.getProperty("user.dir");
	String timestamp = new SimpleDateFormat("yyyy_MM_dd_hh").format(new Date());

	public void passed_TCs(String Passed_Method) throws IOException {
		src_p = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src_p, new File(System.getProperty("user.dir") + "\\test-output\\"
				+ "ScreenShotsTestCases\\Success\\" + timestamp + "\\" + Passed_Method + ".jpg"));

	}

	public void failed_TCs(String Failed_Method) throws IOException {
		src_f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src_f, new File(System.getProperty("user.dir") + "\\test-output\\"
				+ "ScreenShotsTestCases\\Failed\\" + timestamp + "\\" + Failed_Method + ".jpg"));

	}
}
