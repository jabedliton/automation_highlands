package UtilPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestResult;
import BasePages.DataconstantClass;
import TestBase.eTestBase;

public class ExcelReadTestData extends eTestBase {
	public static String s;
	public static  String Url;
	public static String TC_Data;
	public static int num;
	public static String ce;
	public static ITestResult re;
	public static FileInputStream file;
	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;
	public static int totalRow;
	public static int k;
	public static String insCreationStatus;

	public static  String Testdata_String(String Sheetname, String methodName,int k) throws IOException {
		try {
			
			file = new FileInputStream(new File(DataconstantClass.excellocation));
			// Create Workbook instance holding reference to .xlsx file

			workbook = new XSSFWorkbook(file);
			// Get first/desired sheet from the workbook
			sheet = workbook.getSheet(Sheetname);
			// count number of active rows
			totalRow = sheet.getLastRowNum() + 1;
			// count number of active columns in row
			for (int i = 1; i < totalRow; i++) {
				XSSFRow r = sheet.getRow(i);
				ce = r.getCell(1).getStringCellValue();
				if (ce.contains(methodName)) {
					ce=r.getCell(k).getStringCellValue();
					Url = r.getCell(k).getStringCellValue();
					//System.out.println("From excel:" + Url);
					break;
				}
				log.info("Test URL recorded in excel :" + Url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	public static int TestDataRead(String SN, String tc_name,int cell) throws IOException {
		try {
			file = new FileInputStream(new File(DataconstantClass.excellocation));
			// Create Workbook instance holding reference to .xlsx file

			workbook = new XSSFWorkbook(file);
			// Get first/desired sheet from the workbook
			sheet = workbook.getSheet(SN);
			// count number of active rows
			totalRow = sheet.getLastRowNum() + 1;
			// count number of active columns in row
			for (int i = 1; i < totalRow; i++) {
				XSSFRow r = sheet.getRow(i);
				ce = r.getCell(1).getStringCellValue();
				//System.out.println("Tc names : " + ce);
				if (ce.contains(tc_name)) {
					// r.getCell(3).getStringCellValue();
					num = (int) r.getCell(cell).getNumericCellValue();
					System.out.println(insCreationStatus);
					System.out.println(num);
					break;
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return num;

	}
	
	public static void UrlCollection(String ExcelFile, String SN,int k, String passUrl) throws IOException {
        String Tc_Name="Verfiy_eGove_login";
         
		try {
			 file = new FileInputStream(new File(ExcelFile));
			// Create Workbook instance holding reference to .xlsx file
			
			 workbook = new XSSFWorkbook(file);
			// Get first/desired sheet from the workbook
			 sheet = workbook.getSheet(SN);
			// count number of active rows
			int totalRow = sheet.getLastRowNum() + 1;
			// count number of active columns in row
			for (int i = 1; i < totalRow; i++) {
				XSSFRow r = sheet.getRow(i);
				String ce = r.getCell(1).getStringCellValue();
				if (ce.contains(Tc_Name)) {
				//System.out.println(r.getCell(3).getStringCellValue());
					r.createCell(k).setCellValue(passUrl);
					file.close();
					log.info("Result updated in spread sheet ");
					FileOutputStream outFile = new FileOutputStream(new File(ExcelFile));
					workbook.write(outFile);
					outFile.close();
					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
}










