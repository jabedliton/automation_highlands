package UtilPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import TestBase.eTestBase;

public class ExcelResult extends eTestBase {

	public static void updateResult(String ExcelFile, String SheetName,String TC_Name , String TestResult) throws IOException {

		try {
			FileInputStream file = new FileInputStream(new File(ExcelFile));
			// Create Workbook instance holding reference to .xlsx file
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheet(SheetName);
			// count number of active rows
			int totalRow = sheet.getLastRowNum() + 1;
			// count number of active columns in row
			for (int i = 1; i < totalRow; i++) {
				XSSFRow r = sheet.getRow(i);
				String ce = r.getCell(1).getStringCellValue();
				if (ce.contains(TC_Name)) {
				//System.out.println(r.getCell(3).getStringCellValue());
					r.createCell(2).setCellValue(TestResult);
					file.close();
					System.out.println("result updated");
					FileOutputStream outFile = new FileOutputStream(new File(ExcelFile));
					workbook.write(outFile);
					outFile.close();
					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
