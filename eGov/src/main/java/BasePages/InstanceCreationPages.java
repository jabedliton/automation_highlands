package BasePages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class InstanceCreationPages extends eTestBase {
	public String C_url;
	Random rand = new Random();
	@FindBy(xpath = "//input[@id='userPassword']")

	public WebElement PwToSend;
	@FindBy(tagName = "button")
	public WebElement PHPloginBtnClick;
	@FindBy(xpath = "//a[@id='navbarDropdown']")
	public WebElement ManuToClick;
	@FindBy(linkText = "Create")
	public WebElement DropdownCreateToClick;

//Container Generator

	@FindBy(tagName = "select")
	public WebElement InstallDroptoClick;
	@FindBy(xpath = ("//option[contains(text(),'Highlands')]"))
	public WebElement InstallNameToClick;
	@FindBy(xpath = ("//input[@id='container_name']"))
	public WebElement container_nameToSend;

	@FindBy(xpath = ("//button[@id='launch']"))
	public WebElement LaunchButtonToClick;

	@FindBy(xpath = ("//a[contains(text(),'http:')]"))
	public WebElement SiteLinkToClick;

	public InstanceCreationPages() {

		PageFactory.initElements(driver, this);

	}

	public String CreateIns() throws InterruptedException {
		String times = new SimpleDateFormat("yyyyMMdd").format(new Date());
		int T = Integer.valueOf(times)-1;
		log.info("Time Recorded for_db selection :="+ T);
		driver.manage().window().maximize();
		PwToSend.sendKeys(prop.getProperty("password"));
		PHPloginBtnClick.click();
		ManuToClick.click();
		DropdownCreateToClick.click();
		InstallDroptoClick.click();
		Select insname = new Select(driver.findElement(By.xpath("//select[@id='install']")));
		insname.selectByVisibleText(prop.getProperty("Location"));
		container_nameToSend.sendKeys(prop.getProperty("ContainerName") + "_Instance_No_Random_num0");
		driver.findElement(By.xpath("//select[@id='database']")).click();
		String db = prop.getProperty("Location");
		String DbSelection = db.toLowerCase();
		WebElement m = driver.findElement(By.xpath("//select[@id='database']"));
		Select mo = new Select(m);
		List<WebElement> dropdown = mo.getOptions();
		log.info("Entering in loop for data base ");
		for (int i = 0; i < dropdown.size(); i++) {

			if (dropdown.get(i).getText().toLowerCase().contains(DbSelection)) {
				// if
				// (!dropdown.get(i).getText().toLowerCase().contains(prop.getProperty("ProdExclude")))
				// {

//				if (dropdown.get(i).getText().contains("_tc_staging_" + String.valueOf(new TimePicker().TimeDay()))) {
//					String DbSe = dropdown.get(i).getText().toLowerCase();
//					System.out.println("Filtering Db loop =" + new TimePicker().TimeDay() + DbSe);
//					mo.selectByVisibleText(DbSe);
//					// mo.selectByVisibleText((dropdown.get(i).getText()));
//				
//				}
				if (dropdown.get(i).getText().contains("_tc_staging_" + String.valueOf(T))) {
					String DbSe = dropdown.get(i).getText().toLowerCase();
					log.info("Filtered Db  =" + DbSe);
					mo.selectByVisibleText(DbSe);
					
				}

			}

		}
		// @@@@@@@@@@
		driver.findElement(By.xpath("//span[@id='select2-version-container']")).click();
		driver.findElement(By.xpath("//body/span[1]/span[1]/span[1]/input[1]")).sendKeys(prop.getProperty("Branch"));
		driver.findElement(By.xpath("//ul[@id='select2-version-results']")).click();
		LaunchButtonToClick.click();
		log.info(" Switching to iframe");
		driver.switchTo().frame("console");
		wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(prop.getProperty("Con_Site_Url"))));
		SiteLinkToClick.click();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		log.info("Switching out off iframe");
		driver.switchTo().window(tabs2.get(0)).close();
		driver.switchTo().window(tabs2.get(1));
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("submit_btn"))));
			try {
				Assert.assertEquals(driver.findElement(By.id("submit_btn")).isDisplayed(), true);
			} catch (Exception e) {		
				log.info("Db was not selected prior launching site. System is quiting .....");
				driver.quit();	
			}
	
			

		return C_url = driver.getCurrentUrl();
	}

}
