package BasePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class LogInPage extends eTestBase{
	@FindBy(xpath = ("//input[@id='user']"))
	public WebElement userNameTosend;
	@FindBy(xpath = ("//input[@id='pass']"))
	public WebElement pWTosend;

	@FindBy(xpath = ("//select[@id='location_id']"))
	public WebElement LocationIdToclick;
	@FindBy(name= ("location_id"))
	public WebElement LocationIdToSelect;
	@FindBy(xpath = ("//input[@id='submit_btn']"))
	public WebElement ClickonLogInButton;
	@FindBy(xpath = "//img[@alt='eGov']")
	public WebElement LogoCheck;
	@FindBy(xpath = ("//div[@class='profile-pic dropdown']"))
	public WebElement ClickOnProfile;
	@FindBy(xpath=("//a[@class='user-logout']"))
	public WebElement ClickOnLogOut;

	public LogInPage() {

		PageFactory.initElements(driver, this);

	}


	public void Login(String un,String pw) throws InterruptedException {
		userNameTosend.sendKeys(un);
		pWTosend.sendKeys(pw);
		
		try {
			LocationIdToclick.click();
			Select ln = new Select(driver.findElement(By.name("location_id")));
			ln.selectByIndex(1);
		} catch (Exception e) {
			// TODO: handle exception
		}
		ClickonLogInButton.click();
		//wait.until(ExpectedConditions.visibilityOf(LogoCheck));

		//sA.assertEquals(true, LogoCheck.isDisplayed());
		//ClicklOnProfile.click();
		//driver.switchTo().alert().accept();

	}
}
