package Module.Checks.Freeform;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class Check_FreeForm_DomesticesCreateCheck_62 extends eTestBase {
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Checks')]")
	public WebElement Checks;
	@FindBy(xpath = "//a[@href='mod.php?mod=checks&mode=free_form'][contains(.,'Free Form')]")
	public WebElement FreeForm;
	@FindBy(xpath = "//input[contains(@id,'save')]")
	public WebElement Create;
	@FindBy(xpath = "//input[contains(@id,'account')]")
	public WebElement AcctNum;
	@FindBy(xpath = "//input[contains(@id,'price')]")
	public WebElement Price;
	@FindBy(xpath = "(//td[contains(.,'Check Number')])[2]")
	public WebElement gen;
	@FindBy(xpath = "//input[contains(@id,'check_date')]")
	public WebElement CheckDate;
	@FindBy(xpath = "//input[@id='number']")
	public WebElement CheckNumber;
	@FindBy(xpath = "//select[contains(@id,'journal_id')]")
	public WebElement Journal;
	@FindBy(xpath = "//input[contains(@id,'payee')]")
	public WebElement topay;
	@FindBy(xpath = "//*[@id=\"international_address_wrap\"]/input[1]")
	public WebElement Domestic;
	@FindBy(xpath = "//input[contains(@id,'address1')]")
	public WebElement add;
	@FindBy(xpath = "//input[contains(@id,'address2')]")
	public WebElement PO;
	@FindBy(xpath = "//input[contains(@id,'city')]")
	public WebElement city;
	@FindBy(xpath = "//input[contains(@id,'state')]")
	public WebElement STATE;
	@FindBy(xpath = "//input[@id='zip']")
	public WebElement ZcODE;;
	@FindBy(xpath = "//div[contains(@data-allow-outside-click,'false')]")
	public WebElement notification;
	public Check_FreeForm_DomesticesCreateCheck_62() {
		
		PageFactory.initElements(driver, this);
	}
	
	public void CheckFreeForm_Domestic_CreateCheck_62(String a_Num,String Amount,String CheckDateforCheck,String J_ournal) throws InterruptedException {
		 int min = 2;
	      int max = 10;
	      int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
		driver.navigate().refresh();
		Actions ac=new Actions(driver);
		ac.moveToElement(Checks).perform();
		wait.until(ExpectedConditions.elementToBeClickable(FreeForm));
		FreeForm.click();
		wait.until(ExpectedConditions.elementToBeClickable(Create));
		AcctNum.sendKeys(a_Num);
		
	    Price.click();
	    Price.sendKeys(Amount);
	    gen.click();
	   Thread.sleep(5000);
	   wait.until(ExpectedConditions.elementToBeClickable(CheckDate));
	   CheckDate.clear();
	   CheckDate.click();
	   CheckDate.sendKeys(CheckDateforCheck);
	   wait.until(ExpectedConditions.elementToBeClickable(CheckNumber));
	   CheckNumber.click();
	   CheckNumber.sendKeys(String.valueOf(random_int));
	   sc=new Select(Journal);
	   sc.selectByVisibleText(J_ournal);
	   topay.sendKeys("Jabed N Liton");
	   Domestic.click();
	   add.sendKeys("120 WEST MAIN ST");
	   PO.sendKeys("PO BOX 88");
	   city.sendKeys("MAYO");
	   STATE.sendKeys("FL");
	   ZcODE.sendKeys("32066");
	   wait.until(ExpectedConditions.elementToBeClickable(Create));
	   Create.click();
	   wait.until(ExpectedConditions.visibilityOf(notification));
	   
	}
}
