package Module.Checks.Registration;

import static org.testng.Assert.fail;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class Opening_ListedCheck_FromUI_57 extends eTestBase{
	public  Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Checks')]")
	public WebElement check;
	@FindBy(xpath = "//a[@href='mod.php?mod=checks&mode=register'][contains(.,'List Checks')]")
	public WebElement listcheck;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement DateRange;
	@FindBy(xpath = "//div[@class='tableheader'][contains(.,'Check Register')]")
	public WebElement tableHeader;
	@FindBy(xpath = "//input[@id='search']")
	public WebElement search_F10;
	@FindBy(xpath = "//input[contains(@id,'export_csv')]")
	public WebElement Csv_Button;
	@FindBy(linkText ="Click here")
	public WebElement Click;
	@FindBy(xpath = "//input[@id='number']")
	public WebElement CheckRangeFrom;
	@FindBy(xpath = "//input[contains(@id,'number_to')]")
	public WebElement CheckRangeTo;
	@FindBy(xpath = "//tr[@id='row_0']//td[2]")
	public WebElement firstCheck ;
	
	
	@FindBy(xpath = "//tr[@id='row_4']//td[2]")
	public WebElement Last_first ;
	@FindBy(xpath = "//input[contains(@id,'void')]")
	public WebElement VoidCheckButton ;
	public Opening_ListedCheck_FromUI_57() {
	PageFactory.initElements(driver,this);
		
		
	}

public void User_OpeningListedCheck_FromUI_57(String SearchDate,String CheckFrom,String CheckTo) {
	driver.navigate().refresh();
	check.click();
	wait.until(ExpectedConditions.visibilityOf(listcheck));
	listcheck.click();
	wait.until(ExpectedConditions.elementToBeClickable(DateRange));
    sc=new Select(DateRange);
    sc.selectByVisibleText(SearchDate);
    CheckRangeFrom.sendKeys(CheckFrom);
    CheckRangeTo.sendKeys(CheckTo);
    search_F10.click();
    wait.until(ExpectedConditions.visibilityOf(tableHeader));
    String check_F=  firstCheck.getText();
    log.info("First check listed on UI = "+check_F);
    String check_last=  Last_first.getText();
    log.info("Last check listed on UI = "+check_last);
    wait.until(ExpectedConditions.elementToBeClickable(Last_first));
    Last_first.click();
    ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
	log.info("Switching to 2nd window");
	driver.switchTo().window(tabs2.get(0)).close();
	driver.switchTo().window(tabs2.get(1));
	wait.until(ExpectedConditions.elementToBeClickable(VoidCheckButton));
	if(!VoidCheckButton.isDisplayed()) {
		
		Assert.fail();
		log.info("System was not able to open check for viewing");
		
	}else {
		log.info("System was able to open check for viewing");
	}
}


}
