package Module.Checks.Registration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class Check_Reconciliation_ReportByDateRange_Status_IncludeAll_61 extends eTestBase {
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Checks')]")
	public WebElement Checks;
	@FindBy(xpath = "//a[@href='mod.php?mod=checks&mode=check_reconciliation']")
	public WebElement Recon;
	@FindBy(xpath = "//input[contains(@value,'Search (F10)')]")
	public WebElement F10;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement Date_Options;
	@FindBy(xpath = "//*[@id=\"MainCtl\"]/div[2]")
	public WebElement Table;
	@FindBy(xpath = "//select[contains(@name,'reconcile_status')]")
	public WebElement Reconcile_status;
	@FindBy(xpath = "//*[@id=\"MainCtl\"]/div[2]/table/tbody/tr[1]")
	public WebElement Recon_1;
	
	public Check_Reconciliation_ReportByDateRange_Status_IncludeAll_61() {
		
		PageFactory.initElements(driver, this);
	}
	
	public void CheckReconciliationReportByDateRangeStatusIncludeAll_61(String DateRangeSearch,String status) {
		driver.navigate().refresh();
		Actions ac=new Actions(driver);
		ac.moveToElement(Checks).perform();
		wait.until(ExpectedConditions.elementToBeClickable(Recon));
		Recon.click();
		wait.until(ExpectedConditions.elementToBeClickable(F10));
		sc=new Select(Date_Options);
		sc.selectByVisibleText(DateRangeSearch);
	   sc=new Select(Reconcile_status);
	   sc.selectByVisibleText(status);
	   F10.click();
	   wait.until(ExpectedConditions.elementToBeClickable(Table));
	   if(Recon_1.isDisplayed()) {
		   log.info("Check Reconciled column is displayed");
		   Assert.assertTrue(true);
		   
	   }else {
		   log.info("Check Reconciled column is Not displayed");
		   Assert.assertFalse(false);
		
	}
	   
	   
	   
	}
}
