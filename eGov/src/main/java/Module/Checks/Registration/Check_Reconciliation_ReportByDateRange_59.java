package Module.Checks.Registration;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class Check_Reconciliation_ReportByDateRange_59 extends eTestBase {
	@FindBy(xpath = "//a[@href='#'][contains(.,'Checks')]")
	public WebElement Checks;
	@FindBy(xpath = "//a[@href='mod.php?mod=checks&mode=check_reconciliation']")
	public WebElement Recon;
	@FindBy(xpath = "//input[contains(@value,'Search (F10)')]")
	public WebElement F10;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement Date_Options;
	@FindBy(xpath = "//*[@id=\"MainCtl\"]/div[2]")
	public WebElement Table;
	@FindBy(xpath = "//select[contains(@name,'reconcile_status')]")
	public WebElement Reconcile_status;
	@FindBy(xpath = "(//input[contains(@ng-model,'check.clear_date')])[1]")
	public WebElement clearDate_1;
	@FindBy(xpath = "//*[@id=\"MainCtl\"]/div[2]/table/tbody/tr[1]")
	public WebElement Recon_1;
	
	public Check_Reconciliation_ReportByDateRange_59() {
		
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	public void Check_Reconciliation_Report_with_Date_Range_59(String DateRangeSearch) {
		driver.navigate().refresh();
		Actions ac=new Actions(driver);
		ac.moveToElement(Checks).perform();
		wait.until(ExpectedConditions.elementToBeClickable(Recon));
		Recon.click();
		wait.until(ExpectedConditions.elementToBeClickable(F10));
		Select sc=new Select(Date_Options);
		sc.selectByVisibleText(DateRangeSearch);
		F10.click();
		wait.until(ExpectedConditions.elementToBeClickable(Table));
	   
	  
		if(Recon_1.isDisplayed()) {
			   log.info("Check Reconciled column is displayed");
			   Assert.assertTrue(true);
			   
		   }else if(clearDate_1.isDisplayed()) {
			   log.info("Check Unconciled column is  displayed instead of Reconciled column ");
			   Assert.assertFalse(false);
			
		}
	 
	
	
	}


	
}
