package Module.Checks.Registration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class ExportSearched_ListedCheckToCSV_55 extends eTestBase{
	public  Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Checks')]")
	public WebElement check;
	@FindBy(xpath = "//a[@href='mod.php?mod=checks&mode=register'][contains(.,'List Checks')]")
	public WebElement listcheck;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement DateRange;
	@FindBy(xpath = "//div[@class='tableheader'][contains(.,'Check Register')]")
	public WebElement tableHeader;
	@FindBy(xpath = "//input[@id='search']")
	public WebElement search_F10;
	@FindBy(xpath = "//input[contains(@id,'export_csv')]")
	public WebElement Csv_Button;
	@FindBy(linkText ="Click here")
	public WebElement Click;
	public ExportSearched_ListedCheckToCSV_55() {
	PageFactory.initElements(driver,this);
		
		
	}

public void ExportListed_Check_CSV_55(String SearchDate) {
	driver.navigate().refresh();
	check.click();
	wait.until(ExpectedConditions.visibilityOf(listcheck));
	listcheck.click();
	wait.until(ExpectedConditions.elementToBeClickable(DateRange));
    sc=new Select(DateRange);
    sc.selectByVisibleText(SearchDate);
    search_F10.click();
    wait.until(ExpectedConditions.visibilityOf(tableHeader));
    wait.until(ExpectedConditions.elementToBeClickable(Csv_Button));
    Csv_Button.click();
    wait.until(ExpectedConditions.elementToBeClickable(Click));
    Click.click();
    


}


}
