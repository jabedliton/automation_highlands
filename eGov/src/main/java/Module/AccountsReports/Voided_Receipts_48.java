package Module.AccountsReports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class Voided_Receipts_48 extends eTestBase{
	public Actions ac;
	public Select sc;

	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=void_report'][contains(.,'Voided Receipts Report')]")
	public WebElement VR;
	@FindBy(xpath = "(//span[contains(@class,'select2-selection__rendered')])[1]")
	public WebElement QuickDate;
	@FindBy(css = "ul#select2-date_range-results>li:nth-child(3)")
	public WebElement Date;
	@FindBy(xpath = "//input[contains(@id,'dateRange')]")
	public WebElement date;
	@FindBy(xpath = "//span[contains(@id,'select2-receipt_type-container')]")
	public WebElement Module;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[1]")
	public WebElement PM;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[2]")
	public WebElement Created_By;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[3]")
	public WebElement Voided_By;
	@FindBy(xpath = "//button[@id='save']")
	public WebElement search;
	public Voided_Receipts_48(){
		
		PageFactory.initElements(driver, this);
		
		
	}
  public void Voided_Receipts_TC_48(String Index,String Date_Range) throws InterruptedException {
	  driver.navigate().refresh();
	ac=new Actions(driver);
	ac.moveToElement(Receipts).perform();
	VR.click();
	wait.until(ExpectedConditions.elementToBeClickable(search));
	wait.until(ExpectedConditions.elementToBeClickable(QuickDate));
	QuickDate.click();
	
	String s="ul#select2-date_range-results>li:nth-child";
	String ss=s+"("+Index+")";
	WebElement ee=driver.findElement(By.cssSelector(ss));
	System.out.println(ee);
	ee.click();
   date.sendKeys(Date_Range);
	search.click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"listing_wrapper\"]"))));

  
  }
 

}
