package Module.AccountsReports;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import Helper.PDFreader;
import TestBase.eTestBase;

public class Receipt_Group_Breakdown_Report_47 extends eTestBase {
	public Actions ac;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=receipts_group_breakdown_report']")
	public WebElement gr;
	@FindBy(xpath = "//input[@id='from_date']")
	public WebElement fromdate;
	@FindBy(xpath = "//input[@id='to_date']")
	public WebElement dateto;
	@FindBy(xpath = "//input[contains(@id,'export_as_pdf')]")
	public WebElement pdf;
	@FindBy(xpath = "//a[normalize-space()='Clicking Here']")
	public WebElement pdfclick;
	
public Receipt_Group_Breakdown_Report_47() {
	
	PageFactory.initElements(driver, this);
}


public void Verify_Receipt_Group_Breakdown_Report_47(String df,String dt) throws IOException {
	driver.navigate().refresh();
	ac=new Actions(driver);
	ac.moveToElement(receipts).perform();
	wait.until(ExpectedConditions.elementToBeClickable(gr));
	gr.click();
	fromdate.clear();
	fromdate.sendKeys(df);
	dateto.clear();
	dateto.sendKeys(dt);
	pdf.click();
    wait.until(ExpectedConditions.elementToBeClickable(pdfclick));
    pdfclick.click();
    

}




}
