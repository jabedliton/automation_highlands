package Module.AccountsReports;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;


public class Receipts_Employee_Balance_Report_44 extends eTestBase {
	public Select sc;
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=report_employee_balance'][contains(.,'Employee Balance')]")
	public WebElement Employee_Balance;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[1]")
	public WebElement user;
	@FindBy(xpath = "//*[@class=\"select2-results__option select2-results__option--highlighted\"]")
	public WebElement userSelect;
	
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[2]")
	public WebElement PaymentMethod;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[4]")
	public WebElement Group;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[5]")
	public WebElement loc;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[3]")
	public WebElement Module;
	@FindBy(xpath = "//input[contains(@id,'submitbtn')]")
	public WebElement F10;
	@FindBy(xpath = "(//div[@class='tableheader'][contains(.,'Employee Balance')])[1]")
	public WebElement EB;
	@FindBy(xpath = "//input[@id='date_from_to']")
	public WebElement date_from;
	//@FindBy(xpath = "//input[@id='date_to']")
	//public WebElement date_to;
	
	
public Receipts_Employee_Balance_Report_44() {
	
	PageFactory.initElements(driver, this);
}

public void Receipts_Employee_Balance_Report_running() {
	
	Actions ac=new Actions(driver);
	ac.moveToElement(Receipts).perform();
	Employee_Balance.click();
	wait.until(ExpectedConditions.elementToBeClickable(date_from));
	//date_from.clear();
	//date_from.sendKeys(DateFrom);
	//date_to.clear();
	//date_to.sendKeys(DateTo);


	
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].scrollIntoView();", F10);
	F10.click();
	wait.until(ExpectedConditions.elementToBeClickable(EB));
	js.executeScript("arguments[0].scrollIntoView();", EB);

}




}
