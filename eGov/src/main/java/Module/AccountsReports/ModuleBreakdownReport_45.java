package Module.AccountsReports;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class ModuleBreakdownReport_45 extends eTestBase {
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement ele;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=module_breakdown_report']")
	public WebElement breakdownReport;
	@FindBy(xpath = "//input[@id='from_date']")
	public WebElement datefrom;
	@FindBy(xpath = "//input[@id='to_date']")
	public WebElement todate;
	@FindBy(xpath = "//select[contains(@id,'type')]")
	public WebElement Type;
	@FindBy(xpath = "//select[contains(@id,'group_by')]")
	public WebElement gby;
	@FindBy(xpath = "//input[@id='search']")
	public WebElement search;
	@FindBy(xpath = "//th[@class='string sortcol'][contains(.,'Module')]")
	public WebElement Module;
 
	public ModuleBreakdownReport_45() {
	 PageFactory.initElements(driver, this);
	 
 }


public void ModuleBreakdownReportFunctionlity_RunReport(String dateFrom,String dateTo, String TYPE,String GroupBy  ) {
	
	Actions ac=new Actions(driver);
	ac.moveToElement(ele).perform();
	breakdownReport.click();
	wait.until(ExpectedConditions.elementToBeClickable(datefrom));
	datefrom.clear();
	datefrom.sendKeys(dateFrom);
	todate.clear();
	todate.sendKeys(dateTo);
	sc=new Select(Type);
	sc.selectByVisibleText(TYPE);
	sc=new Select(gby);
	sc.selectByVisibleText(GroupBy);
	search.click();
	wait.until(ExpectedConditions.visibilityOf(Module));
	
}


}
