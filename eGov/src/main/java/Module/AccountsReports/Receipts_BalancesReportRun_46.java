package Module.AccountsReports;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class Receipts_BalancesReportRun_46 extends eTestBase {
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=report_receipt_balances']")
	public WebElement Re_ba;
	@FindBy(xpath = "//input[@id='date_from']")
	public WebElement dateFrom;
	@FindBy(xpath = "//input[@id='date_to']")
	public WebElement dateTo;
	@FindBy(xpath = "//select[contains(@id,'user_id')]")
	public WebElement User;
	@FindBy(xpath = "//div[@class='tableheader'][contains(.,'Receipt Balances Report')]")
	public WebElement table;
	@FindBy(xpath = "//input[contains(@id,'submitbtn')]")
	public WebElement F10;
	
	public Receipts_BalancesReportRun_46() {
	 PageFactory.initElements(driver, this);
 }

	public void ReceiptsBalanceReportRunning(String dateF,String DateT,String user) {
		Actions ac=new Actions(driver);
		driver.navigate().refresh();
		ac.moveToElement(Receipts).perform();
		wait.until(ExpectedConditions.elementToBeClickable(Re_ba));
		Re_ba.click();
		wait.until(ExpectedConditions.elementToBeClickable(dateFrom));
		dateFrom.clear();
		dateFrom.sendKeys(dateF);;
		dateTo.clear();
		dateTo.sendKeys(DateT);
		Select s=new Select(User);
		s.selectByVisibleText(user);
		F10.click();
		wait.until(ExpectedConditions.visibilityOf(table));
		
	}
	
}
