package Module.Receipts.ReceiptsListing;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2;
import TestBase.eTestBase;

public class ReceiptsListing_38 extends eTestBase {

	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement receipts;
	@FindBy(xpath = "//a[contains(.,'Receipt Listing')]")
	public WebElement ReceiptsListing;
	@FindBy(xpath = "//button[contains(.,'Search (F10)')]")
	public WebElement SearchButton;
	@FindBy(xpath = "//input[contains(@id,'receipt_id')]")
	public WebElement Receipts_ID;
	@FindBy(xpath = "//button[@ng-click='searchReceipts()']")
	public WebElement button;
	@FindBy(xpath = "//input[contains(@ng-model,'search_criteria.receipt_id')]")
	public WebElement reid2;
	@FindBy(xpath = "//*[@id=\"listing\"]/tbody/tr/td[3]")
	public WebElement renumberonUI;
	@FindBy(xpath = "//*[@id=\"listing\"]/tbody/tr/td[13]")
	public WebElement Check;
	@FindBy(xpath = "//*[@id=\"listing\"]/tbody/tr/td[11]")
	public WebElement UC_Total;
	@FindBy(xpath = "//input[contains(@name,'id')]")
	public WebElement reid1;
	public ReceiptsListing_38() {
		PageFactory.initElements(driver, this);
	}

	public void ReceiptsListingFunctionality() throws InterruptedException {
		PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2 n = new PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2();
		n.PropertyTax_OnePaymentMethod_GL_Total_Trans("100");
		driver.navigate().refresh();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", receipts);
		Actions ac = new Actions(driver);
		ac.moveToElement(receipts).perform();
		ReceiptsListing.click();
		wait.until(ExpectedConditions.elementToBeClickable(SearchButton));
		wait.until(ExpectedConditions.elementToBeClickable(reid2));

		String Receipts = n.GlrNum;
		log.info("Global receipts number is=" + Receipts);
		js.executeScript("arguments[0].scrollIntoView();", button);
		// @@element not intractable
		Thread.sleep(3000);
		try {
			reid2.click();
			reid2.sendKeys(Receipts);
		} catch (Exception e) {
			// TODO: handle exception
			reid1.click();
			reid1.sendKeys(Receipts);
		}
		

		SearchButton.click();

		if (Receipts.contains(renumberonUI.getText())) {
			log.info("Receipts ="+Receipts +" matched with UI receipts Number ="+renumberonUI.getText());
        Assert.assertTrue(true);
		}else {
			log.info("Receipts ="+Receipts +"did not matched with UI receipts Number ="+renumberonUI.getText());
			Assert.assertFalse(false);
		}
		String checkAmountUI=Check.getText().replaceAll("[$,]", "");
		if(checkAmountUI.contains(n.UC_Total)) {
			log.info(checkAmountUI+" =Check amount on UI Matched with UC paid amount= "+n.UC_Total);
			Assert.assertTrue(true);
		}else {
			log.info(checkAmountUI + " =Check amount on UI did not  Match with UC paid amount= "+n.UC_Total);
			Assert.assertTrue(false);
		}
	}

}
