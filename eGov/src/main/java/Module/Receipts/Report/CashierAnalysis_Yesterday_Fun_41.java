package Module.Receipts.Report;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class CashierAnalysis_Yesterday_Fun_41 extends eTestBase {
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=report_cashier_analysis']")
	public WebElement CA;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement dr;
	@FindBy(xpath = "//select[contains(@id,'location_id')]")
	public WebElement LOC;
	@FindBy(xpath = "//input[contains(@name,'run_report')]")
	public WebElement RunReport;
	@FindBy(xpath = "//*[@id=\"data\"]")
	public WebElement dataTable;
	
	
	
	
	public CashierAnalysis_Yesterday_Fun_41() {

		PageFactory.initElements(driver, this);

	}

public void cashierAnalysisFun(String DateRange,String loc) {

driver.navigate().refresh();
JavascriptExecutor js = (JavascriptExecutor) driver;
js.executeScript("arguments[0].scrollIntoView();", receipts);
Actions ac = new Actions(driver);
ac.moveToElement(receipts).perform();
wait.until(ExpectedConditions.elementToBeClickable(CA));
CA.click();
wait.until(ExpectedConditions.elementToBeClickable(dr) );
sc=new Select(dr);
sc.selectByVisibleText(DateRange);
sc=new Select(LOC);
sc.selectByVisibleText(loc);
RunReport.click();
wait.until(ExpectedConditions.visibilityOf(dataTable));

}}
