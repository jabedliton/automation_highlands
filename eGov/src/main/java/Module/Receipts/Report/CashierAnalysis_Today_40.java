package Module.Receipts.Report;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class CashierAnalysis_Today_40 extends eTestBase {
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement receipts;
	@FindBy(xpath = "//a[@href='mod.php?mod=receipts&mode=report_cashier_analysis']")
	public WebElement CA;
	@FindBy(xpath = "//select[contains(@id,'date_range')]")
	public WebElement dr;
	@FindBy(xpath = "//select[contains(@id,'location_id')]")
	public WebElement LOC;
	@FindBy(xpath = "//input[contains(@name,'run_report')]")
	public WebElement RunReport;
	@FindBy(xpath = "//*[@id=\"data\"]")
	public WebElement dataTable;
	
	
	
	
	public CashierAnalysis_Today_40() {

		PageFactory.initElements(driver, this);

	}

public void cashierAnalysisFun(String dateRange,String loc) {

driver.navigate().refresh();
System.out.println("exe");
JavascriptExecutor js = (JavascriptExecutor) driver;
js.executeScript("arguments[0].scrollIntoView();", receipts);
Actions ac = new Actions(driver);
ac.moveToElement(receipts).perform();
try {
	wait.until(ExpectedConditions.elementToBeClickable(CA));
	CA.click();
} catch (Exception e) {
	// TODO: handle exception
	driver.findElement(By.linkText("Clicking Here")).click();
}

wait.until(ExpectedConditions.elementToBeClickable(dr) );
sc=new Select(dr);
sc.selectByVisibleText(dateRange);
sc=new Select(LOC);
sc.selectByVisibleText(loc);
RunReport.click();
wait.until(ExpectedConditions.visibilityOf(dataTable));

}}
