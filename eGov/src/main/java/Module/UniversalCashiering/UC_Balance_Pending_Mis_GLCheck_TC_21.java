package Module.UniversalCashiering;

import java.util.Random;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class UC_Balance_Pending_Mis_GLCheck_TC_21 extends eTestBase {
	public JavascriptExecutor js;
	public String RefNumber;
	@FindBy(css = ".fa-dollar-sign")
	public WebElement UcLink;
	// @FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//*[@id=\"misc_cancel_sec\"]")
	public WebElement CancelButton;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@ng-model='misChks.chkBalPending']")
	public WebElement CheckBoxClick;
	@FindBy(xpath = "//input[@id='bp_amount']")
	public WebElement BPamount;
	@FindBy(xpath = "//input[@id='bp_reference_id']")
	public WebElement BPRef;
	@FindBy(xpath = "//select[contains(@id,'bp_label')]")
	public WebElement BPLabel;
	@FindBy(xpath = "//select[@id='bp_type']")
	public WebElement BPtype;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'name')][@name='bp_name']")
	public WebElement bp_name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//td[contains(.,'Cashier Item: MIS')]")
	public WebElement MIS;
	@FindBy(xpath = "//button[contains(@id,'misc_submit_sec')]")
	public WebElement MISSubmitButton;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;
	public UC_Balance_Pending_Mis_GLCheck_TC_21() {

		PageFactory.initElements(driver, this);

	}

	public void UC_BPending_Mis_Bal_TC_21(String amount, String Cheknum, String Name) throws InterruptedException {
		Random rand = new Random(); //instance of random class
		int min = 345;
	      int max = 599;
	        //generate random values from 0-99
	      int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
	    RefNumber =String.valueOf(random_int);
		UcLink.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(Miscellaneous));
		Miscellaneous.click();
		//with $5 dollars default value
		//Notary.click();
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", CheckBoxClick);
		//js.executeScript("window.scrollBy(0,1000)");
		CheckBoxClick.click();
		BPamount.sendKeys(amount);
		BPtype.click();
		Select BT = new Select((BPtype));
		BT.selectByVisibleText("Property Tax (PPT)");
		bp_name.sendKeys("Jabed Liton");
		BPRef.sendKeys(RefNumber);
		log.info("RefNumber for balance pending search ="+ RefNumber);
		BPLabel.click();
		Select BP = new Select((BPLabel));
		BP.selectByVisibleText("BK PAYMENT");
		MISSubmitButton.click();
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		
		wait.until(ExpectedConditions.visibilityOfAllElements(Amount));
		Amount.sendKeys(amount);
		Check.sendKeys(Cheknum);
	    name.sendKeys(Name);
		F10.click();
	try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		wait.until(ExpectedConditions.visibilityOf(MIS));
		wait.until(ExpectedConditions.elementToBeClickable(GlobalReceipts));
		GlobalReceipts.click();
		wait.until(ExpectedConditions.elementToBeClickable(paidamountGl));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", paidamountGl);

		// @@@verify that GL total is the same is was paid in UC
		if (paidamountGl.getText().replaceAll("[$,]", "").contains(amount)) {

			log.info("Total Payment under GL:"+paidamountGl.getText());
			log.info("Paid balance did  match in GL");
		} else {

			Assert.fail("paid balance did not match in GL");
		}
		;
		// @@Verify that under trans#debit credit balance is correct

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", translink);
		wait.until(ExpectedConditions.elementToBeClickable(translink));
		translink.click();
		if (DebitAmount.getText().contains((amount))) {
			log.info("Paid amount did match DebitAmount ");
			Assert.assertTrue(true);
		} else {

			Assert.fail();
		}

		if (CreditAmount.getText().contains(amount)) {
			log.info("Paid amount did match Credit amount ,"
					+ " Hence Debit and credit is same  ");
			Assert.assertTrue(true);
		} else {

			Assert.fail("Paid amount did not match Credit amount , " +
			"Hence Debit and credit is not the same ");
		}

	}

	}
		
				

	


