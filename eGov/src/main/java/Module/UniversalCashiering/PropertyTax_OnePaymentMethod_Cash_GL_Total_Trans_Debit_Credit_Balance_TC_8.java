package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import TestBase.eTestBase;
import org.testng.Assert;

import Helper.GenerateRandom;

@SuppressWarnings("deprecation")
public class PropertyTax_OnePaymentMethod_Cash_GL_Total_Trans_Debit_Credit_Balance_TC_8 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//select[@ng-model='payment.method_key']")
	public WebElement Forcash;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;

	public PropertyTax_OnePaymentMethod_Cash_GL_Total_Trans_Debit_Credit_Balance_TC_8() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void PropertyTax_OnePaymentMethod_Cash_(
			String BillDueAmount) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Account selected for payment is =" + accountnum.getAttribute("value"));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		log.info("Balance due to be paind under UC ="+due);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Forcash.click();
		Select P = new Select(Forcash);
		P.selectByVisibleText("CASH");
		wait.until(ExpectedConditions.elementToBeClickable(Amount));
		Amount.sendKeys(due);
		wait.until(ExpectedConditions.elementToBeClickable(name));
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));
		GlobalReceipts.click();
		// @@@GL Total balance >Trans# Debit credit balance
		wait.until(ExpectedConditions.elementToBeClickable(paidamountGl));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", paidamountGl);

		// @@@verify that GL total is the same is was paid in UC
		if (paidamountGl.getText().replaceAll("[$,]", "").contains(due)) {

			log.info("Total Recorded Under Global Receipts :" + paidamountGl.getText());
			log.info("Paid balance did  match in GL");
		} else {

			Assert.fail("paid balance did not match in GL");
		}
		;
		// @@Verify that under trans#debit credit balance is correct

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", translink);
		wait.until(ExpectedConditions.elementToBeClickable(translink));
		translink.click();
		if (DebitAmount.getText().contains((due))) {
			log.info("Paid amount did match DebitAmount ");
			Assert.assertTrue(true);
		} else {
			log.info("Paid amount did match Not  DebitAmount ");
			Assert.fail();
		}

		if (CreditAmount.getText().contains(due)) {
			log.info("Paid amount did match Credit amount , Hence Debit and credit is same  ");
			Assert.assertTrue(true);
		} else {

			Assert.fail("Paid amount did not match Credit amount , " + "Hence Debit and credit is not the same ");
		}

	}
}