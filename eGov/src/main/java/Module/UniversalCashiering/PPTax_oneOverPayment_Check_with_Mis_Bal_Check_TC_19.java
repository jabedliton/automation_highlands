package Module.UniversalCashiering;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PPTax_oneOverPayment_Check_with_Mis_Bal_Check_TC_19 extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement Overpayment_verification;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_1')]")
	public WebElement Amount1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	public WebElement check;
	@FindBy(xpath = "(//input[contains(@ng-model,'payment.check_number')])[4]")
	public WebElement check1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkNotary')]")
	public WebElement Notary;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'payment_name_1')]")
	public WebElement name1;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//*[@id=\"misc_submit_sec\"]")
	public WebElement saveButtonMis;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])[2]")
	public WebElement paymentMethod2;
	@FindBy(xpath = "//span[contains(@ng-show,'due >= 0 && totals.overpayment == 0')]")
	WebElement RemainingBalance;

	public PPTax_oneOverPayment_Check_with_Mis_Bal_Check_TC_19() {

		PageFactory.initElements(driver, this);
	}

	public void PPTax_PaymentMethod_WithMis_overpayment(
			String BillDueAmount, double overPayment) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Accound is selected for pay is = " + accountnum.getAttribute("value"));
		UC.click();
		// @@@@AddingMis to the Bill@@@@@@@@@@@@@
		Miscellaneous.click();
		wait.until(ExpectedConditions.elementToBeClickable(Notary));
		try {
			Notary.click();
		} catch (Exception e) {
			log.info("Catch block executed for to click on Notary CheckBox");
			driver.findElement(By.xpath("(//input[@required='required'])[1]")).click();
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButtonMis);
		saveButtonMis.click();
		// @@@@
		try {
			js.executeScript("arguments[0].scrollIntoView();", Amount);
		} catch (Exception e) {
			// TODO: handle exception
			js.executeScript("arguments[0].scrollIntoView();", saveButton);
		}
//		Select ln = new Select((paymentMethod1));
//		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		log.info("Under UC bill to be paid - Mis $5 dollars =" + due);
		// 5 dollars over payment
		// Double overPayment = 5.00;
		// data conversation
		Amount.clear();
		Amount.click();
		Amount.sendKeys(String.valueOf(Double.valueOf(due) + overPayment));
		checkid2.sendKeys("09");
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(Overpayment_verification));

		// @@Balance Check under Transaction Tab
		try {
			// for MultiYear Bill
			BillNum_Link.click();
		} catch (Exception e) {
			// for single year bill
			driver.findElement(By.xpath("//*[@id=\"newQRCtrl\"]//tbody//td[@class=" + "'data ng-binding']//a")).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		
	}

}
