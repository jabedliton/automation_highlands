package Module.UniversalCashiering;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;
public class UC_BP_PayingWith_BP_tc_23 extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(css = ".fa-dollar-sign")
	public WebElement UcLink;
	// @FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkNotary')]")
	public WebElement Notary;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement MIS;
	@FindBy(xpath = "//select[@ng-model='payment.method_key']")
	public WebElement PM;
	@FindBy(xpath = "//input[contains(@id,'bpSearch')]")
	public WebElement BP;
	@FindBy(xpath = "(//select[contains(@aria-invalid,'false')])[18]")
	public WebElement BP_ac;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement Overpayment_verification;
	@FindBy(xpath = "//a[contains(text(),'Receipt#')]")
	public WebElement UC_BlancePendingLink;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	public UC_BP_PayingWith_BP_tc_23() {

		PageFactory.initElements(driver, this);

	}

	public void UC_PayingWithBalancePendins_TC_23(String paidReceipts,String amount) throws InterruptedException {

		UcLink.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(Miscellaneous));
		Miscellaneous.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(Notary));
		//with $5 dollars default value
		Notary.click();
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		saveButton.click();
		try {
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			// TODO: handle exception
			js.executeScript("window.scrollBy(0,1000)");
		}
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		wait.until(ExpectedConditions.visibilityOfAllElements(Amount));
		PM.click();
		Select drp = new Select(PM);
		drp.selectByVisibleText("BALANCE PENDING");
		Amount.sendKeys(amount);
		BP_ac.click();
		Select dr = new Select(BP_ac);
		dr.selectByVisibleText("DEPOSITS-BALANCE PENDING");
		BP.click();
		BP.sendKeys(paidReceipts);
		BP.click();
		BP.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.elementToBeClickable(UC_BlancePendingLink));
		UC_BlancePendingLink.click();
		F10.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));

	}

}
