package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Default_Check_PostMarking_TC_3 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;

	public PropertyTax_OnePaymentMethod_Default_Check_PostMarking_TC_3() {

		PageFactory.initElements(driver, this);
	}

	public void PropertyTax_OnePaymentMethod_CheckPostmarking(
			String BillDueAmount,String PostMarkDate) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		PostmarkingDate.click();
		In = new Select(PostmarkingDate);
		In.selectByVisibleText(PostMarkDate);
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		Amount.sendKeys(due);
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(completed_receipt));

		try {
			BillNum_Link.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[7]"));
		String PostMarkedAs= l.getText();
		log.info("PostMarkedAs ="+PostMarkedAs);
		if (PostMarkedAs.contains("POSTMARKED")) {
			log.info("Passed-Bill balance is POSTMARKED");

		} else {
			log.info("Passed-Bill  is Not POSTMARKED");

			Assert.fail("Bill is not POSTMARKED");
		}

	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//@@Old code@@@@@@@

//		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
//		int row = 0;
//		int index = 6;
//		for (row = 0; row < table.size(); row++) {
//			List<WebElement> col = table.get(row).findElements(By.tagName("td"));
//			@SuppressWarnings("unlikely-arg-type")
//			String PostMarked_Sealed = col.get(index).getText();
//			if (PostMarked_Sealed.contains("POSTMARKED")) {
//				log.info("Postmarked is found in Grid is  "+PostMarked_Sealed);
//				break;
//			}
//           
//		}
//
//		System.out.println("Size of row count in table " + table.size());
	}


