package Module.UniversalCashiering;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	public String GlrNum;
	public String UC_Total;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]/input")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//input[contains(@name,'submit_btn')]")
	public WebElement submitNSF;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;

	public PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2() {
		//
		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void PropertyTax_OnePaymentMethod_GL_Total_Trans(
			String BillDueAmount) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));

		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		wait.until(ExpectedConditions.elementToBeClickable(Amount));
		Amount.sendKeys(due);
		log.info("Amount due in UC ="+due);
		UC_Total=due;
		wait.until(ExpectedConditions.elementToBeClickable(check));
		try {
			wait.until(ExpectedConditions.elementToBeClickable(check));
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.elementToBeClickable(GlobalReceipts));
		GlrNum=GlobalReceipts.getText();
		GlobalReceipts.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(paidamountGl));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", paidamountGl);

		// @@@verify that GL total is the same is was paid in UC
		if (paidamountGl.getText().replaceAll("[$,]", "").contains(due)) {

			log.info("Total Payment under GL:"+paidamountGl.getText());
			log.info("Paid balance did  match in GL");
		} else {

			Assert.fail("paid balance did not match in GL");
		}
		;
		// @@Verify that under trans#debit credit balance is correct

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", translink);
		wait.until(ExpectedConditions.elementToBeClickable(translink));
		translink.click();
		if (DebitAmount.getText().contains((due))) {
			log.info("Paid amount did match DebitAmount ");
			Assert.assertTrue(true);
		} else {

			Assert.fail();
		}

		if (CreditAmount.getText().contains(due)) {
			log.info("Paid amount did match Credit amount ,"
					+ " Hence Debit and credit is same  ");
			Assert.assertTrue(true);
		} else {

			Assert.fail("Paid amount did not match Credit amount , " + "Hence Debit and credit is not the same ");
		}
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

}
