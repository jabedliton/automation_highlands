package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Default_Check_ReturnCheck_TC_4 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@type='text'])[87]")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//img[@alt='Returned Check']")
	public WebElement Return;
	@FindBy(xpath = "//input[@id='note']")
	public WebElement notes;
	@FindBy(xpath = "//input[contains(@name,'submit_btn')]")
	public WebElement submitNSF;
	@FindBy(xpath = "//a[normalize-space()='Transactions']")
	public WebElement Trans;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Parcel History')]")
	public WebElement PHistory;
	@FindBy(xpath = "//td[contains(text(),'RETURNED CHECK')] ")
	public WebElement ReturnCheck;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement ac;
	@FindBy(xpath = "(//a[contains(@aria-hidden,'false')])[1]")
	public WebElement BillNum_Link2;

	public PropertyTax_OnePaymentMethod_Default_Check_ReturnCheck_TC_4() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings("static-access")
	public void PropertyTax_OnePaymentMethod_ReturnCheck() throws InterruptedException {
		// @@Return check workflow
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		wait.until(ExpectedConditions.elementToBeClickable(Return));
		Return.click();
		wait.until(ExpectedConditions.elementToBeClickable(notes));
		notes.sendKeys("test");
		submitNSF.click();
		js.executeScript("window.scrollBy(0,500)");
		wait.until(ExpectedConditions.visibilityOf(ReturnCheck));
		log.info("Account number is  =" + ac.getAttribute("value"));
		WebElement l = driver.findElement(By.xpath("//table[@id='data']/tbody/tr[last()-5]/td[14]"));
        log.info("new balance after NSF IS =" + l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();

		if (!BillBalance.contains(bi)) {
			log.info("Passed-Returned check bill with Balance ");
             Assert.assertTrue(true);
		} else {
			log.info("Failed-Returned check bill with no Balance");

			Assert.fail();
		}

	}

}
