package Module.UniversalCashiering;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_Partial_PaymentMethod_GLR_Total_Debit_Credit_Check_TC_16 extends eTestBase {
	public JavascriptExecutor js;
	public String Bill_Yp;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//label[contains(.,'Bill Type')]")
	public WebElement billtype;
	@FindBy(xpath = "//span[contains(@id,'select2-status_flag-container')]")
	public WebElement flag;
	@FindBy(xpath = "//input[contains(@class,'select2-search__field')]")
	public WebElement searchFields;
	@FindBy(xpath = "//span[contains(@title,'NA-DO NOT ADVERTISE')]")
	public WebElement DonotAdvertise;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "(//input[contains(@ng-model,'bill.amount_to_pay.total')])[1]")
	public WebElement forPartiaLPayment;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[contains(.,'Receipt Processed Successfully')]")
	public WebElement Partial_verification;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;

	public PropertyTax_Partial_PaymentMethod_GLR_Total_Debit_Credit_Check_TC_16() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings("static-access")
	public void PropertyTax_PartialPaymentMethod(String SearchYear, String OperatorSign_Forbalance,
			String BillDueAmount, String PaymentMethod, String PartialPayment) throws InterruptedException {

		Actions actions = new Actions(driver);

		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		wait.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
		EnterTaxYear.click();
		FilterTaxYear.sendKeys(SearchYear);
		SelectTaxYear.click();
		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
		BalanceAmount.sendKeys(BillDueAmount);
		//js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].scrollIntoView();", billtype);
		//flag.click();
		//searchFields.sendKeys("NA-DO NOT ADVERTISE");
		//driver.findElement(By.xpath("//li[contains(.,'NA-DO NOT ADVERTISE')]")).click();
		ClickOnSearch.click();
		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
		// @SuppressWarnings("unlikely-arg-type")
		@SuppressWarnings("unlikely-arg-type")
		String Bill_Amount = col.get(14).getText();
		// Float Billtype=Float.valueOf(col.get(2).getText());
		col.get(2).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(forPartiaLPayment));
		// @@@@@@@@@@@UC@@@@@@@@@@@@@@@@@@
		try {

			forPartiaLPayment.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		} catch (Exception e) {

		}
		try {
			// @@if it is single Bill
			if (driver.findElement(By.xpath(
					"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
					.isDisplayed()) {
				driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
						.clear();
				driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
						.sendKeys(PartialPayment);
				Bill_Yp = driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"
								+ "//parent::div//preceding-sibling::div//input[@id='quick_search']"))
						.getAttribute("value");
			}
		} catch (Exception e) {
			// @@if it is MultiYears Bill
		
			driver.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']")).clear();
			driver.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']"))
					.sendKeys(PartialPayment);
			Bill_Yp = driver
					.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']"
							+ "//parent::div//preceding-sibling::div//input[@id='quick_search']"))
					.getAttribute("value");
			log.info("Catch Block Executed");
		}
		// forPartiaLPayment.sendKeys("20");

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Amount.click();
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		Amount.sendKeys(String.valueOf(Double.valueOf(due)));
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(Partial_verification));
		GlobalReceipts.click();
		wait.until(ExpectedConditions.elementToBeClickable(paidamountGl));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", paidamountGl);

		// @@@verify that GL total is the same is was paid in UC
		
		if (paidamountGl.getText().replaceAll("[$,]", "").
				contains(String.valueOf(Double.valueOf(due)))) {

			log.info("GLR Total ="+paidamountGl.getText());
			log.info("Paid balance did  match in GL");
		} else {

			Assert.fail("paid balance did not match in GL");
		}
		;
		// @@Verify that under trans#debit credit balance is correct

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", translink);
		wait.until(ExpectedConditions.elementToBeClickable(translink));
		translink.click();
		if (DebitAmount.getText().contains((String.valueOf(Double.valueOf(due) )))) {
			log.info("Paid amount did match DebitAmount ");
			Assert.assertTrue(true);
		} else {
			log.info("Paid amount did Not match DebitAmount ");
			Assert.fail();
		}

		if (CreditAmount.getText().contains((String.valueOf(Double.valueOf(due))))) {
			log.info("Paid amount did match Credit amount , Hence Debit and credit is same  ");
			Assert.assertTrue(true);
		} else {

			Assert.fail("Paid amount did not match Credit amount , " 
			+ "Hence Debit and credit is not the same ");
		}
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	}


