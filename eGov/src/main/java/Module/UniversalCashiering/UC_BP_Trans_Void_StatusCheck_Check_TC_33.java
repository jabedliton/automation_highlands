package Module.UniversalCashiering;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class UC_BP_Trans_Void_StatusCheck_Check_TC_33 extends eTestBase {
	public JavascriptExecutor js;
	public String RefNumber;
	@FindBy(css = ".fa-dollar-sign")
	public WebElement UcLink;
	// @FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//*[@id=\"misc_cancel_sec\"]")
	public WebElement CancelButton;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@ng-model='misChks.chkBalPending']")
	public WebElement CheckBoxClick;
	@FindBy(xpath = "//input[@id='bp_amount']")
	public WebElement BPamount;
	@FindBy(xpath = "//input[@id='bp_reference_id']")
	public WebElement BPRef;
	@FindBy(xpath = "//select[contains(@id,'bp_label')]")
	public WebElement BPLabel;
	@FindBy(xpath = "//select[@id='bp_type']")
	public WebElement BPtype;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'name')][@name='bp_name']")
	public WebElement bp_name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//td[contains(.,'Cashier Item: MIS')]")
	public WebElement MIS;
	@FindBy(xpath = "//button[contains(@id,'misc_submit_sec')]")
	public WebElement MISSubmitButton;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;

	@FindBy(xpath = "(//a[contains(@class,'ng-binding')])[1]")
	public WebElement PR;
	@FindBy(xpath = "//input[@id='void_receipt']")
	public WebElement vR;
	@FindBy(xpath = "//textarea[contains(@id,'reason')]")
	public WebElement Reason;
	@FindBy(xpath = "//input[contains(@id,'submit_btn')]")
	public WebElement Submit;
	@FindBy(xpath = "//span[contains(.,'VOID')]")
	public WebElement voidStatus;
	@FindBy(xpath = "//div[contains(text(),'VOIDED')]")
	public WebElement vOID_r;

	public String RefNumber_Link;
	@FindBy(xpath = "(//input[contains(@ng-model,'colFilter.term')])[3]")
	public WebElement RefIdFilter;
	@FindBy(xpath = "//button[@id='bp_submit_search']")
	public WebElement bp_submit_search;
	@FindBy(xpath = "(//a[contains(.,'Balance Pendings')])[1]")
	public WebElement BalancePendings;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "(//a[contains(@class,'ng-binding')])[1]")
	public WebElement BPRef_number;
	@FindBy(xpath = "(//div[contains(@class,'ui-grid-cell-contents ng-binding ng-scope')])[4]")
	public WebElement Ref_id;

	public UC_BP_Trans_Void_StatusCheck_Check_TC_33() {

		PageFactory.initElements(driver, this);

	}

	public void UC_BP_void_StatusCheck_TC_33(String ID, String Status) throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOf(PR));
		PR.click();
		wait.until(ExpectedConditions.elementToBeClickable(vR));
		vR.click();
		wait.until(ExpectedConditions.elementToBeClickable(Reason));
		Reason.click();
		Reason.sendKeys("Test");
		Submit.click();
		wait.until(ExpectedConditions.visibilityOf(voidStatus));

		Receipts.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(BalancePendings));
		BalancePendings.click();
		wait.until(ExpectedConditions.elementToBeClickable(RefIdFilter));
		RefIdFilter.click();
		RefIdFilter.sendKeys(ID);
		try {
			bp_submit_search.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		bp_submit_search.click();
		log.info(vOID_r.getText() + " is New  BP status");
		if (vOID_r.getText().contains(Status)) {

			log.info(Status + " status  was Found For Void Receipts");
			Assert.assertTrue(true);

		} else {
			log.info(Status + " status  was Not Found For Void Receipts ");
			Assert.fail();
		}
		;
		// @@@@@@@@@@@@@@@And@@@@@@@@@@

	}
}
