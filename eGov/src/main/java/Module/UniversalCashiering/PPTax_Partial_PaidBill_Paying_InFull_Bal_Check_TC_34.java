package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PPTax_Partial_PaidBill_Paying_InFull_Bal_Check_TC_34 extends eTestBase {
	public JavascriptExecutor js;
	public String Bill_Yp;
	public String AcctNum;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//label[contains(.,'Bill Type')]")
	public WebElement billtype;
	@FindBy(xpath = "//span[contains(@id,'select2-status_flag-container')]")
	public WebElement flag;
	@FindBy(xpath = "//input[contains(@class,'select2-search__field')]")
	public WebElement searchFields;
	@FindBy(xpath = "//span[contains(@title,'NA-DO NOT ADVERTISE')]")
	public WebElement DonotAdvertise;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "(//input[contains(@ng-model,'bill.amount_to_pay.total')])[1]")
	public WebElement forPartiaLPayment;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[contains(.,'Receipt Processed Successfully')]")
	public WebElement Partial_verification;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//*[@id=\"tabPane1\"]/div[1]/h2[2]/a")
	public WebElement Transaction;
	@FindBy(xpath = "(//b[contains(.,'P & I')])[2]")
	public WebElement toclick;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;

	@FindBy(xpath = "//*[@id=\"tax_account_id\"]")
	public WebElement AccoundNumber;
	
	public PPTax_Partial_PaidBill_Paying_InFull_Bal_Check_TC_34() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings("static-access")
	public void Partial_PaidBill_Paying_InFull_Bal_Check_TC_34(String PaymentMethod) throws InterruptedException {
		
       //@@@@@@@@@@@@@@@@@@@@@@@@@
		AcctNum=AccoundNumber.getText();
		log.info("Account Number ="+ AcctNum);
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		//List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
		// @SuppressWarnings("unlikely-arg-type")
//		@SuppressWarnings("unlikely-arg-type")
//		String Bill_Amount = col.get(14).getText();
//		// Float Billtype=Float.valueOf(col.get(2).getText());
//		col.get(2).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		
		// @@@@@@@@@@@UC@@@@@@@@@@@@@@@@@@

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		Amount.click();
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		Amount.sendKeys(String.valueOf(Double.valueOf(due)));
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));
		try {
			// for MultiYear Bill
			driver.findElement(By.xpath("//*[@id=\"newQRCtrl\"]//tbody//td[@class=" + "'data ng-binding']//a"))
			.click();
		} catch (Exception e) {
			// for single year bill
			BillNum_Link.click();
			
		}
		 
	       //get the height of the webpage and scroll to the end
	       js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

         //@@Transaction Tab checking for Partial Payment
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		js = (JavascriptExecutor) driver;  
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		System.out.println(l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();
		System.out.println(BillBalance);
		if (BillBalance.contains(bi)) {
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Failed-Bill balance is Not Zero");

			Assert.fail("Balance is not Zero");
		}
	}
	}


