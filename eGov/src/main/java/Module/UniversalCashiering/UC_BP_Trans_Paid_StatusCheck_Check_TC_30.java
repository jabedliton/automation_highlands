package Module.UniversalCashiering;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import TestBase.eTestBase;

public class UC_BP_Trans_Paid_StatusCheck_Check_TC_30 extends eTestBase {
	public JavascriptExecutor js;
	public String RefNumber;
	@FindBy(css = ".fa-dollar-sign")
	public WebElement UcLink;
	// @FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//*[@id=\"misc_cancel_sec\"]")
	public WebElement CancelButton;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@ng-model='misChks.chkBalPending']")
	public WebElement CheckBoxClick;
	@FindBy(xpath = "//input[@id='bp_amount']")
	public WebElement BPamount;
	@FindBy(xpath = "//input[@id='bp_reference_id']")
	public WebElement BPRef;
	@FindBy(xpath = "//select[contains(@id,'bp_label')]")
	public WebElement BPLabel;
	@FindBy(xpath = "//select[@id='bp_type']")
	public WebElement BPtype;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'name')][@name='bp_name']")
	public WebElement bp_name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//td[contains(.,'Cashier Item: MIS')]")
	public WebElement MIS;
	@FindBy(xpath = "//button[contains(@id,'misc_submit_sec')]")
	public WebElement MISSubmitButton;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;
	@FindBy(xpath = "//div[contains(text(),'PAID')]")
	public WebElement Paid;
	
	public UC_BP_Trans_Paid_StatusCheck_Check_TC_30() {

		PageFactory.initElements(driver, this);

	}

	public void UC_BP_PaidStatusCheck_TC_30(String Status
			) throws InterruptedException {
		
		wait.until(ExpectedConditions.visibilityOf(Paid));
		log.info(Paid.getText() + " is Newly added BP status");
		if(Paid.getText().contains(Status)){
			
			log.info(Status + " was Found for new added balance pending Entry");
		   Assert.assertTrue(true);
			
		}else {
			log.info(Status + " was Not Found for new added balance pending Entry");
			Assert.fail();
		};
		//@@@@@@@@@@@@@@@And@@@@@@@@@@
		
		

	}

		
		
		
	}


