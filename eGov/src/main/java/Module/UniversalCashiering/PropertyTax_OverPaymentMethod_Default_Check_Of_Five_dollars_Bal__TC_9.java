package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_Bal__TC_9 extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@type='text'])[87]")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement Overpayment_verification;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;

	public PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_Bal__TC_9() {

		PageFactory.initElements(driver, this);
	}

	public void PropertyTax_OverPaymentMethod_5_dollars_TC_9(
			String BillDueAmount, String PaymentMethod,double overPayment) throws InterruptedException {

//		Actions actions = new Actions(driver);
//		actions.moveToElement(ClickOnPropertyTax).perform();
//		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
//		ClickOnSearchBillNew.click();
//		wait.until(ExpectedConditions.elementToBeClickable(
//				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
//		EnterTaxYear.click();
//		FilterTaxYear.sendKeys(SearchYear);
//		SelectTaxYear.click();
//		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
//		BalanceAmount.sendKeys(BillDueAmount);
//		ClickOnSearch.click();
//		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
//		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
//		List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
//		// @SuppressWarnings("unlikely-arg-type")
//		@SuppressWarnings("unlikely-arg-type")
//		String Bill_Amount = col.get(14).getText();
//		// Float Billtype=Float.valueOf(col.get(2).getText());
//		col.get(2).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		log.info("Under UC bill to be paid ="+due);
		// 5 dollars over payment
		//Double overPayment = 5.00;
		// data conversation
		Amount.sendKeys(String.valueOf(Double.valueOf(due) + overPayment));
		checkid2.sendKeys("09");
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(Overpayment_verification));

		//@@Balance Check under Transaction Tab
		BillNum_Link.click();
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		log.info("Balance under balance column in webtable =" + l.getText());
		String OverpaymentEntryMessage = driver
				.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-6]/td[9]")).getText();
		if (OverpaymentEntryMessage.contains("OVER UNDER PAYMENT")) {
			log.info("OVER UNDER PAYMENT entry message found in Grid");
		} else {

			Assert.fail("OVER UNDER PAYMENT entry message found in Grid");

		}
		String bi = "0.00";
		String BillBalance = l.getText();
		log.info("Bill balance is =" + BillBalance);

		if (BillBalance.contains(bi)) {
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Passed-Bill balance is Not Zero");

			Assert.fail("Balance is not Zero");
		}

	}

}
