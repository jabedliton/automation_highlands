package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import TestBase.eTestBase;
import org.testng.Assert;

import Helper.GenerateRandom;

@SuppressWarnings("deprecation")
public class PPTax_OnePayment_OP_AmountVerification_TC_29 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	public Actions actions;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//div[@class='ng-modal show ng-isolate-scope']//table[@class='ng-scope']")
	public WebElement OP_Pop_Up;
	@FindBy(xpath = "//table[@class='ng-scope']//tbody//tr//td//input[@value='OK']")
	public WebElement Ok_Button;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//a[contains(.,'Overpayment/Refund Tool')]")
	public WebElement OPTool;
	
	
	//table[@class='ng-scope']//tbody//tr//td//input[@value='OK']
	public PPTax_OnePayment_OP_AmountVerification_TC_29() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void PropertyTax_OP_AmountVerification_TC_29(
			String BillDueAmount, double overPayment) throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Accound is selected for pay is = " + accountnum.getAttribute("value"));
		UC.click();
		// @@@@@@@@@@@@@@@@@
		
		wait.until(ExpectedConditions.elementToBeClickable(Tdue));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		//log.info("Under UC bill to be paid - Mis $5 dollars =" + due);
		// 5 dollars over payment
		// Double overPayment = 5.00;
		// data conversation
		Amount.clear();
		Amount.click();
		Amount.sendKeys(String.valueOf(Double.valueOf(due) + overPayment));
		checkid2.click();
		wait.until(ExpectedConditions.elementToBeClickable(Ok_Button));
		Ok_Button.click();
		wait.until(ExpectedConditions.visibilityOf(checkid2));
		checkid2.sendKeys("09");
		name.sendKeys("Jabed Liton");
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));
       String GLR=GlobalReceipts.getText();
		log.info("GLR Number for OP Tool ="+GLR);
		log.info("OP Amount was added with Balance ="+overPayment);
		actions=new Actions(driver);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(OPTool));
		OPTool.click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"form\"]/h2[1]"))));
		String GLRNum=driver.findElement(By.xpath("//*[@id=\"tablekit-table-1\"]//tbody/tr[last()]/td[3]")).getText();
		String OPAmount=driver.findElement(By.xpath("//*[@id=\"tablekit-table-1\"]//tbody/tr[last()]/td[13]")).getText();
		try {
			js.executeScript("window.scrollBy(0,1000)", "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		js.executeScript("window.scrollBy(0,1000)", "");
		
		if(GLRNum.contains(GLR)) {
			log.info("Under OP tool Global Trans Number ="+GLRNum + "Matched with GLR ="+GLR );
			Assert.assertTrue(true);
			
			
		}else {
			log.info("GLR transaction Number miss match");
			Assert.assertFalse(false);
		}
		
		if(OPAmount.contains(String.valueOf(overPayment))) {
			log.info("OPAmount ="+ OPAmount+"Did Matched with UC op Amount " + overPayment  );
			Assert.assertTrue(true);
			
		}else{
			log.info("OPAmount ="+ OPAmount+"Did not Matched with UC op Amount " + overPayment  );
			Assert.assertFalse(false);
			
		}
	    
		
	}

	}
	
