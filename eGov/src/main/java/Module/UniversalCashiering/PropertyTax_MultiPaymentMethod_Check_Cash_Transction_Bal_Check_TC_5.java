package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_MultiPaymentMethod_Check_Cash_Transction_Bal_Check_TC_5 extends eTestBase {
	public JavascriptExecutor js;
	public String AccountNumber;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile']"
			+ "[contains(.,'Bill Information')]")
	public WebElement BillInfo;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountIDdpay; 
	@FindBy(xpath = "//input[contains(@id,'search_exact_btn')]")
	public WebElement searchExactbtn; 
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_1')]")
	public WebElement Amount1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	public WebElement check;
	@FindBy(xpath = "(//input[contains(@ng-model,'payment.check_number')])[4]")
	public WebElement check1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'payment_name_1')]")
	public WebElement name1;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])[2]")
	public WebElement paymentMethod2;
	@FindBy(xpath = "//span[contains(@ng-show,'due >= 0 && totals.overpayment == 0')]")
	WebElement RemainingBalance;

	public PropertyTax_MultiPaymentMethod_Check_Cash_Transction_Bal_Check_TC_5() {

		PageFactory.initElements(driver, this);
	}
	public void PropertyTax_MultiPaymentMethod_Default(
			String BillDueAmount, String CheckPaymentMethod1,String CashPaymentMethod2) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		AccountNumber=accountnum.getAttribute("value");
		log.info("Accound is selected for pay is = "+AccountNumber);
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(CheckPaymentMethod1);
//		String due = Tdue.getText();
//		due = due.replaceAll("[$,]", "");
		Amount.sendKeys(BillDueAmount);
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
	//@@@@@
		paymentMethod2.click();
		Select P = new Select((paymentMethod2));
	    P.selectByVisibleText(CashPaymentMethod2);
		String RemainingBa = RemainingBalance.getText();
		System.out.println(RemainingBa);
		RemainingBa = RemainingBa.replaceAll("[$,]", "");
		Amount1.sendKeys(RemainingBa);
		
    	name1.sendKeys("Jabed Liton");
		
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		//@@Functionality Verified 
		wait.until(ExpectedConditions.visibilityOf(completed_receipt));
         //@@Transaction Tab for balance Zero check
		BillNum_Link.click();
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		//Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		System.out.println(l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();
		System.out.println(BillBalance);

		if (BillBalance.contains(bi)) {
			Assert.assertTrue(true);
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Passed-Bill balance is Not Zero");
			Assert.fail("Balance is not Zero");
		}

	}
		
		
		
		
		
		
		
		
	}


