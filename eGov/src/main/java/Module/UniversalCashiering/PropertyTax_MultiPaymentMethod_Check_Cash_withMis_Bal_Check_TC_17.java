package Module.UniversalCashiering;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_MultiPaymentMethod_Check_Cash_withMis_Bal_Check_TC_17 extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//*[@id=\"enter_quickreceipt_button\"]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_1')]")
	public WebElement Amount1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	public WebElement check;
	@FindBy(xpath = "(//input[contains(@ng-model,'payment.check_number')])[4]")
	public WebElement check1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkNotary')]")
	public WebElement Notary;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'payment_name_1')]")
	public WebElement name1;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//*[@id=\"misc_submit_sec\"]")
	public WebElement saveButtonMis;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])[2]")
	public WebElement paymentMethod2;
	@FindBy(xpath = "//span[contains(@ng-show,'due >= 0 && totals.overpayment == 0')]")
	WebElement RemainingBalance;
	@FindBy(xpath = "//body/div[@id='outer']/div[@id='page']/div[@id='content']"
			+ "/div[@id='newQRCtrl']/div/div[@class='d-table']" + "/div[@class='d-row']/div[@class='d-cell second']"
			+ "/div[@class='bills_to_pay_div']/fieldset[1]")
	WebElement UCLoad;

	public PropertyTax_MultiPaymentMethod_Check_Cash_withMis_Bal_Check_TC_17() {

		PageFactory.initElements(driver, this);
	}

	public void PropertyTax_MultiPaymentMethod_Default(String BillDueAmount, String CheckPaymentMethod1,
			String CashPaymentMethod2) throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(UC));

		// TODO: handle exception
		driver.navigate().refresh();
		log.info("Accound is selected for pay is = " + accountnum.getAttribute("value"));
		UC.click();

		// @@@@AddingMis to the Bill@@@@@@@@@@@@@
		try {
			wait.until(ExpectedConditions.visibilityOf(UCLoad));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("thread.sleep executed for to load UC");
			log.info("thread.sleep executed for to load UC");
			Thread.sleep(7000);
		}

		Miscellaneous.click();
		wait.until(ExpectedConditions.elementToBeClickable(Notary));
		try {
			Notary.click();
		} catch (Exception e) {
			log.info("Catch block executed for to click on Notary CheckBox");
			driver.findElement(By.xpath("(//input[@required='required'])[1]")).click();
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButtonMis);
		saveButtonMis.click();
		// @@@@
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		paymentMethod1.click();
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(CheckPaymentMethod1);
		// @@Using below 2 clicks for to Load Due amount
		Amount.clear();
		Amount.click();
		Thread.sleep(5000);
		Amount.sendKeys(BillDueAmount);
		try {

			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}

		name.sendKeys("Jabed Liton");
		// @@@@@
		paymentMethod2.click();
		Select P = new Select((paymentMethod2));
		P.selectByVisibleText(CashPaymentMethod2);
		String RemainingBa = RemainingBalance.getText();
		System.out.println(RemainingBa);
		RemainingBa = RemainingBa.replaceAll("[$,]", "");
		// System.out.println(RemainingBa);
		Amount1.sendKeys(RemainingBa);
		name1.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		// @@Functionality Verified
		wait.until(ExpectedConditions.visibilityOf(completed_receipt));
		// @@Transaction Tab for balance Zero check
		try {
			BillNum_Link.click();
		} catch (Exception e) {
			driver.findElement(By.xpath("//*[@id=\"newQRCtrl\"]" + "//table//tbody//tr[4]//td[2]//a")).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		System.out.println(l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();
		System.out.println(BillBalance);

		if (BillBalance.contains(bi)) {
			Assert.assertTrue(true);
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Passed-Bill balance is Not Zero");
			Assert.fail("Balance is not Zero");
		}

	}

}
