package Module.UniversalCashiering;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_MultiPaymentMethod_Check_Cash_withMis_GL_DebitCredit_TC_18 extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_1')]")
	public WebElement Amount1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	public WebElement check;
	@FindBy(xpath = "(//input[contains(@ng-model,'payment.check_number')])[4]")
	public WebElement check1;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkNotary')]")
	public WebElement Notary;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'payment_name_1')]")
	public WebElement name1;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath ="//*[@id=\"misc_submit_sec\"]")
	public WebElement saveButtonMis;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])[2]")
	public WebElement paymentMethod2;
	@FindBy(xpath = "//span[contains(@ng-show,'due >= 0 && totals.overpayment == 0')]")
	WebElement RemainingBalance;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']"
			+ "/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "//*[@id=\"content\"]/table[2]/tbody/tr[last()]/td[3]/span/span")
	public WebElement paidamountGl;
	@FindBy(xpath = "//strong[contains(.,'Trans #')]")
	public WebElement trans;
	@FindBy(xpath = "//a[contains(@href,'transaction_id=')]")
	public WebElement translink;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[3]")
	public WebElement DebitAmount;
	@FindBy(xpath = "//*[@id='entries_table']/tr[@class='entry_row'][last()]/td[4]")
	public WebElement CreditAmount;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;
	@FindBy(xpath = "//*[@id=\"payments_total\"]//span")
	public WebElement totaldue;
	//*[@id="payments_total"]//span

	public PropertyTax_MultiPaymentMethod_Check_Cash_withMis_GL_DebitCredit_TC_18() {

		PageFactory.initElements(driver, this);
	}
	public void PropertyTax_MultiPaymentMethod_Default(String BillDueAmount, String CheckPaymentMethod1,String CashPaymentMethod2) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Accound is selected for pay is = "+accountnum.getAttribute("value"));
		UC.click();
		//@@@@AddingMis to the Bill@@@@@@@@@@@@@
		Miscellaneous.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(Notary));
		//with $5 dollars default value
		Notary.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButtonMis);
		saveButtonMis.click();	
		//@@@@
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(CheckPaymentMethod1);
		
		wait.until(ExpectedConditions.visibilityOfAllElements(totaldue));
		System.out.println(totaldue.getText());
		if(totaldue.getText().contains("$5.")) {
			System.out.println("Thread executed.....");
			Thread.sleep(7000);		
		}
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		Amount.clear();
		Amount.click();
		Amount.sendKeys(BillDueAmount);
		//@@T 
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
	//@@@@@
		paymentMethod2.click();
		Select P = new Select((paymentMethod2));
	    P.selectByVisibleText(CashPaymentMethod2);
		String RemainingBa = RemainingBalance.getText();
		System.out.println(RemainingBa);
		RemainingBa = RemainingBa.replaceAll("[$,]", "");
		//System.out.println(RemainingBa);
		
		Amount1.sendKeys(RemainingBa);
    	name1.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}	
         //@@GLR TotalCheck
		wait.until(ExpectedConditions.elementToBeClickable(GlobalReceipts));
		GlobalReceipts.click();
		wait.until(ExpectedConditions.elementToBeClickable(paidamountGl));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", paidamountGl);
		
		// @@@verify that GL total is the same it was paid in UC
				if (paidamountGl.getText().replaceAll("[$,]", "").contains(due)) {

					log.info("Total paid amount under GL= "+paidamountGl.getText());
		            log.info("Paid balance did  match in GL");
				} else {
					log.info("Total paid amount under GL= "+paidamountGl.getText());
					System.out.println(paidamountGl.getText());
					Assert.fail("paid balance did not match in GL");
				};
				// @@Verify that under trans#debit credit balance is correct
				js.executeScript("arguments[0].scrollIntoView();", translink);
				wait.until(ExpectedConditions.elementToBeClickable(translink));
				translink.click();
				if (DebitAmount.getText().contains((due))) {
					 log.info("Paid amount did match DebitAmount ");
					Assert.assertTrue(true);
				}else {
					
					Assert.fail();
				}
				if (CreditAmount.getText().contains(due)) {
					log.info("Paid amount did match Credit amount , "
							+ "Hence Debit and credit is same  ");
					Assert.assertTrue(true);
				}else {
					
					Assert.fail("Paid amount did not match Credit amount , "
							+ "Hence Debit and credit is not the same ");
				}
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			}
		
		
	}
		
		
		
			
	
