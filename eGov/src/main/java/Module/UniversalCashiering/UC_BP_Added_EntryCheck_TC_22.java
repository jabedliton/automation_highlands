package Module.UniversalCashiering;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import TestBase.eTestBase;

public class UC_BP_Added_EntryCheck_TC_22 extends eTestBase {
	public JavascriptExecutor js;
	public String RefNumber_Link;
	@FindBy(xpath = "(//input[contains(@ng-model,'colFilter.term')])[3]")
	public WebElement RefIdFilter;
	@FindBy(xpath = "//button[@id='bp_submit_search']")
	public WebElement bp_submit_search;
	@FindBy(xpath = "(//a[contains(.,'Balance Pendings')])[1]")
	public WebElement BalancePendings;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "(//a[contains(@class,'ng-binding')])[1]")
	public WebElement BPRef_number;
	@FindBy(xpath = "(//div[contains(@class,'ui-grid-cell-contents ng-binding ng-scope')])[4]")
	public WebElement Ref_id;
	
	public UC_BP_Added_EntryCheck_TC_22() {

		PageFactory.initElements(driver, this);

	}	
	public void UC_BalancePendingAddedEntryCheck_TC_22(String ID) throws InterruptedException {
		Receipts.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(BalancePendings));
		BalancePendings.click();
		wait.until(ExpectedConditions.elementToBeClickable(RefIdFilter));
		RefIdFilter.click(); 
		RefIdFilter.sendKeys(ID);
		try {
			bp_submit_search.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		bp_submit_search.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(BPRef_number));
		System.out.println("---refText="+Ref_id.getText());
		RefNumber_Link=BPRef_number.getText();
		//System.out.println("---linkText"+BPRef_number.getText());
		
		if(Ref_id.getText().contains(ID)) {
			log.info("Ref id for balance Pending is matched >"+Ref_id.getText()+"="+ID);
			Assert.assertTrue(true);
			
		}else
		{
			log.info("Ref id for balance Pending did not  match >"+Ref_id+"="+ID);
		   Assert.assertFalse(false);	
			
		}

		
	}

}
