package Module.UniversalCashiering;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.sun.javafx.image.BytePixelSetter;

import TestBase.eTestBase;

public class UC_BP_Trans_Disbursment_StatusCheck_Check_TC_32 extends eTestBase {
	public JavascriptExecutor js;
	public String RefNumber;
	@FindBy(css = ".fa-dollar-sign")
	public WebElement UcLink;
	// @FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkMiscellaneous')]")
	public WebElement MisCheck;
	@FindBy(xpath = "//*[@id=\"misc_cancel_sec\"]")
	public WebElement CancelButton;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@ng-model='misChks.chkBalPending']")
	public WebElement CheckBoxClick;
	@FindBy(xpath = "//input[@id='bp_amount']")
	public WebElement BPamount;
	@FindBy(xpath = "//input[@id='bp_reference_id']")
	public WebElement BPRef;
	@FindBy(xpath = "//select[contains(@id,'bp_label')]")
	public WebElement BPLabel;
	@FindBy(xpath = "//select[@id='bp_type']")
	public WebElement BPtype;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "//input[contains(@id,'name')][@name='bp_name']")
	public WebElement bp_name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//td[contains(.,'Cashier Item: MIS')]")
	public WebElement MIS;
	@FindBy(xpath = "//button[contains(@id,'misc_submit_sec')]")
	public WebElement MISSubmitButton;
	@FindBy(xpath = "//a[contains(@title,'Universal Cashiering')]")
	public WebElement UC;
	@FindBy(xpath = "//b[contains(.,'Total:')]")
	public WebElement total;
	@FindBy(xpath = "//div[contains(text(),'DISBURSED')]")
	public WebElement Disbursed;
	
	public String RefNumber_Link;
	@FindBy(xpath = "(//input[contains(@ng-model,'colFilter.term')])[3]")
	public WebElement RefIdFilter;
	@FindBy(xpath = "//button[@id='bp_submit_search']")
	public WebElement bp_submit_search;
	@FindBy(xpath = "(//a[contains(.,'Balance Pendings')])[1]")
	public WebElement BalancePendings;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement Receipts;
	@FindBy(xpath = "(//a[contains(@class,'ng-binding')])[1]")
	public WebElement BPRef_number;
	@FindBy(xpath = "(//div[contains(@class,'ui-grid-cell-contents ng-binding ng-scope')])[4]")
	public WebElement Ref_id;
	@FindBy(xpath = "//input[contains(@id,'paymentMisc')]")
	public WebElement mispayment;
	@FindBy(xpath = "//select[@ng-model='payment.method_key']")
	public WebElement PM;
	@FindBy(xpath = "//input[contains(@id,'bpSearch')]")
	public WebElement BP;
	@FindBy(xpath = "(//select[contains(@aria-invalid,'false')])[18]")
	public WebElement BP_ac;
	@FindBy(xpath = "//a[contains(text(),'Receipt#')]")
	public WebElement UC_BlancePendingLink;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	@FindBy(xpath = "(//div[@class='ui-grid-cell-contents text-right ng-binding ng-scope'])[1]")
	public WebElement BPAmount;
	@FindBy(xpath = "(//div[@class='ui-grid-cell-contents text-right ng-binding ng-scope'])[2]")
	public WebElement PaidAmount;
	public UC_BP_Trans_Disbursment_StatusCheck_Check_TC_32() {

		PageFactory.initElements(driver, this);

	}

	public void UC_BP_DisbursedStatusCheck_TC_32(double amount,String paidReceipts,String ID,String Status
			) throws InterruptedException {
		//@@@@@CreatingMisBill@@@@@@@@@@@
		 wait.until(ExpectedConditions.elementToBeClickable(UC));
		 UC.click();
		wait.until(ExpectedConditions.elementToBeClickable(Miscellaneous));
		Miscellaneous.click();
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", MisCheck);
		MisCheck.click();
		mispayment.clear();
		mispayment.click();
		mispayment.sendKeys(String.valueOf(amount));
		js.executeScript("arguments[0].scrollIntoView(true);", saveButton);
		saveButton.click();
		
		try {
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			// TODO: handle exception
			js.executeScript("window.scrollBy(0,1000)");
		}
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		wait.until(ExpectedConditions.visibilityOfAllElements(Amount));
		PM.click();
		Select drp = new Select(PM);
		drp.selectByVisibleText("BALANCE PENDING");
		Amount.click();
		Amount.sendKeys(String.valueOf(amount));
		BP_ac.click();
		Select dr = new Select(BP_ac);
		dr.selectByVisibleText("DEPOSITS-BALANCE PENDING");
		BP.click();
		BP.sendKeys(paidReceipts);
		BP.click();
		BP.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.elementToBeClickable(UC_BlancePendingLink));
		UC_BlancePendingLink.click();
		F10.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));	
	
	//@@@@BPModule@@@@@@@@@@@@@@@@@@	
		Receipts.click();
		wait.until(ExpectedConditions.visibilityOfAllElements(BalancePendings));
		BalancePendings.click();
		wait.until(ExpectedConditions.elementToBeClickable(RefIdFilter));
		RefIdFilter.click(); 
		RefIdFilter.sendKeys(ID);
		try {
			bp_submit_search.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		bp_submit_search.click();
		wait.until(ExpectedConditions.visibilityOf(Disbursed));
		log.info(Disbursed.getText() + " is  BP status");
		if(Disbursed.getText().contains(Status)){
			
			log.info(Status + " Status was Found ");
		   Assert.assertTrue(true);
			
		}else {
			log.info(Status + " Status was Not Found ");
			Assert.fail();
		};
		double Tamount=Double.valueOf(BPAmount.getText());
			
		double DisAmount=Double.valueOf(PaidAmount.getText());
			if(Tamount==DisAmount){
				log.info("Total amount "+ Tamount +" is matched with " + DisAmount +" Disbursed Amount" );
				Assert.assertTrue(true);
				
			}else {
				log.info("Total amount "+ Tamount +" Did not matched with" + DisAmount );
				Assert.fail();
			}
			
		}
			
			
		
		//@@@@@@@@@@@@@@@And@@@@@@@@@@
		
		

	}

		
		
		
	


