package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import TestBase.eTestBase;
import org.testng.Assert;

import Helper.GenerateRandom;

@SuppressWarnings("deprecation")
public class PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_TC_26 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//div[@class='ng-modal show ng-isolate-scope']//table[@class='ng-scope']")
	public WebElement OP_Pop_Up;
	@FindBy(xpath = "//table[@class='ng-scope']//tbody//tr//td//input[@value='OK']")
	public WebElement Ok_Button;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	
	
	//table[@class='ng-scope']//tbody//tr//td//input[@value='OK']
	public PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_TC_26() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void PPTax_OnePayment_Overpayment_OPAmount_Check_TC_26(
			String BillDueAmount, double overPayment) throws InterruptedException {

//		Actions actions = new Actions(driver);
//		actions.moveToElement(ClickOnPropertyTax).perform();
//		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
//		ClickOnSearchBillNew.click();
//		wait.until(ExpectedConditions.elementToBeClickable(
//				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
//		EnterTaxYear.click();
//		FilterTaxYear.sendKeys(SearchYear);
//		SelectTaxYear.click();
//		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
//		BalanceAmount.sendKeys(BillDueAmount);
//		ClickOnSearch.click();
//		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
//		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
//		List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
//		// @SuppressWarnings("unlikely-arg-type")
//		@SuppressWarnings("unlikely-arg-type")
//		String Bill_Amount = col.get(14).getText();
//		// Float Billtype=Float.valueOf(col.get(2).getText());
//		col.get(2).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Accound is selected for pay is = " + accountnum.getAttribute("value"));
		UC.click();
		// @@@@@@@@@@@@@@@@@
		
		wait.until(ExpectedConditions.elementToBeClickable(Tdue));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", Amount);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		log.info("Under UC bill to be paid - Mis $5 dollars =" + due);
		// 5 dollars over payment
		// Double overPayment = 5.00;
		// data conversation
		Amount.clear();
		Amount.click();
		Amount.sendKeys(String.valueOf(Double.valueOf(due) + overPayment));
		checkid2.click();
		wait.until(ExpectedConditions.elementToBeClickable(Ok_Button));
		Ok_Button.click();
		wait.until(ExpectedConditions.visibilityOf(checkid2));	
		checkid2.sendKeys("09");
		name.sendKeys("Jabed Liton");
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));

		// @@Balance Check under Transaction Tab
		try {
			// for MultiYear Bill
			BillNum_Link.click();
		} catch (Exception e) {
			// for single year bill
	driver.findElement(By.xpath("//*[@id=\"newQRCtrl\"]"
					+ "//tbody//td[@class=" + "'data ng-binding']//a")).click();
	}
		wait.until(ExpectedConditions.visibilityOf(Transaction));
		Transaction.click();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		log.info("Balance under balance column in webtable =" + l.getText());
		String OverpaymentEntryMessage = driver
		.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-6]/td[9]")).getText();
		
		String OverpaymentAmount = driver
				.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-6]/td[12]")).getText();
				
		if (OverpaymentEntryMessage.contains("OVERPAYMENT")) {
			log.info("OVERPAYMENT entry message found in Grid");
			Assert.assertTrue(true);
		} else {
         Assert.fail("OVERPAYMENT entry message was not found in Grid");

		}
		
		if(OverpaymentAmount.contains(String.valueOf(overPayment))) {
			log.info("Over payment Amount of "+ String.valueOf(overPayment)+" matched and found in Grid");
			Assert.assertTrue(true);
		}else {
			 Assert.fail("Over payment Amount did not matched and found in Grid");
			
		}
		String bi = "0.00";
		String BillBalance = l.getText();
		log.info("Bill balance is =" + BillBalance);

		if (BillBalance.contains(bi)) {
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Passed-Bill balance is Not Zero");

			Assert.fail("Balance is not Zero");
		}

		
		
		
		
		
		
		
		
		
		
		
		
	}


	}
