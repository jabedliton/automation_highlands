package Module.UniversalCashiering;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Default_Check extends eTestBase {
	public JavascriptExecutor js;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@type='text'])[87]")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public  WebElement paymentMethod1;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;

	public PropertyTax_OnePaymentMethod_Default_Check() {

		PageFactory.initElements(driver, this);
	}

	public void PropertyTax_OnePaymentMethod_Default(String SearchYear, String OperatorSign_Forbalance,
			String BillDueAmount,String PaymentMethod) throws InterruptedException {

		Actions actions = new Actions(driver);
		Thread.sleep(2000);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		wait.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
		EnterTaxYear.click();
		Thread.sleep(3000);
		FilterTaxYear.sendKeys(SearchYear);
		SelectTaxYear.click();
		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
		BalanceAmount.sendKeys(BillDueAmount);
		ClickOnSearch.click();
		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		List<WebElement> col = table.get(2).findElements(By.tagName("td"));
		// @SuppressWarnings("unlikely-arg-type")
		@SuppressWarnings("unlikely-arg-type")
		String Bill_Amount = col.get(14).getText();
		// Float Billtype=Float.valueOf(col.get(2).getText());
		col.get(2).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due=due.replaceAll("[$,]" , "");
		Thread.sleep(4000);
		Amount.sendKeys(due);
		Thread.sleep(4000);
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		Thread.sleep(3000);
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(completed_receipt));

		//@@@@@@VerifyBillhasNobalanceUnderTrnsactionTab@@@@@@@@@
		try {
			BillNum_Link.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		Thread.sleep(3000);
         System.out.println(driver.findElement(By.xpath("//table[@id='data']/tbody/tr[1]/td[12]")).getText());
		
			}

		 //System.out.println("Size of row count in table " + table.size());
	}
		
		
		
		
		
		
	


