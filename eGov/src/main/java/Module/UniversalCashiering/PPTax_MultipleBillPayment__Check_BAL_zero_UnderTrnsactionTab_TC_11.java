package Module.UniversalCashiering;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import TestBase.eTestBase;
import org.testng.Assert;
import org.testng.util.TimeUtils;

@SuppressWarnings("deprecation")
public class PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;

	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//div[@class='action-bar-grey']" + "//a[contains(text(),'Universal Cashiering')]")
	public WebElement UC;

//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "//div[@class='d-table']//input[@value='(F2) Search']")
	public WebElement Search;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//*[@class='bills_to_pay_div']")
	public WebElement TableLoad;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;

	public PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public void PPTax_MultiPayment_CheckBalance(String SearchYear, String OperatorSign_Forbalance,
			String BillDueAmount) throws InterruptedException {

		Actions actions = new Actions(driver);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		wait.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
		EnterTaxYear.click();
		FilterTaxYear.sendKeys(SearchYear);
		SelectTaxYear.click();
		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
		BalanceAmount.sendKeys(BillDueAmount);
		ClickOnSearch.click();
		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
		// List<WebElement> table =
		// driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		driver.findElement(By.xpath("//table[@id='data']/tbody/tr[1]/td[1]")).click();
		driver.findElement(By.xpath("//table[@id='data']/tbody/tr[2]/td[1]")).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0)).close();
		driver.switchTo().window(tabs2.get(1));
		try {
			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", Amount);
		} catch (Exception e) {
			// TODO: handle exception
			Actions at = new Actions(driver);
			at.sendKeys(Keys.PAGE_DOWN).build().perform();
		}

		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		wait.until(ExpectedConditions.elementToBeClickable(Amount));
		Amount.click();
		Amount.sendKeys(due);
		wait.until(ExpectedConditions.elementToBeClickable(check));
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(completed_receipt));
		BillNum_Link.click();
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		log.info("Balance Found after full Payment is =" + l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();
		System.out.println(BillBalance);

		if (BillBalance.contains(bi)) {
			log.info("Passed-Bill balance is Zero after full payment");

		} else {
			log.info("Passed-Bill balance is Not Zero after full payment");

			Assert.fail("Balance is not Zero");
		}

	}
}