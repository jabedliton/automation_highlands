package Module.UniversalCashiering;

import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class UC_BP_Trans_Bal_Check_AfterFullPayment_TC_24 extends eTestBase {
	public JavascriptExecutor js;
	public Select In;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=billfile'][contains(.,'Bill Information')]")
	public WebElement BillInformation;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement AccountId;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = "//input[@id='search_button']")
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//input[contains(@id,'tax_account_id')]")
	public WebElement accountnum;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//h1[@ng-show='completed_receipt']")
	public WebElement completed_receipt;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//select[@ng-model='bill.postmark']")
	public WebElement PostmarkingDate;
	@FindBy(xpath = "(//td[contains(@ng-show,'isNumeric(payment.bill_id)')])[2]")
	public WebElement BillNum_Link;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "//a[contains(@href,'id= ')]")
	public WebElement GR;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//div[@class='ng-binding'][contains(.,'MiscellaneousMIS')]")
	public WebElement Miscellaneous;
	@FindBy(xpath = "//input[contains(@ng-model,'misChks.chkNotary')]")
	public WebElement Notary;
	@FindBy(id = "misc_submit_sec")
	public WebElement saveButton;
	@FindBy(xpath = "//input[@id='payment_amount_0']")
	public WebElement Amount;
	@FindBy(xpath = "(//input[@size='10'])[32]")
	public WebElement Check;
	@FindBy(xpath = "//input[@ng-model='payment.name']")
	public WebElement name;
	@FindBy(xpath = "(//input[contains(@value,'(F10) Finish Receipt')])[2]")
	public WebElement F10;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement MIS;
	@FindBy(xpath = "//select[@ng-model='payment.method_key']")
	public WebElement PM;
	@FindBy(xpath = "//input[contains(@id,'bpSearch')]")
	public WebElement BP;
	@FindBy(xpath = "(//select[@ng-model='payment.account_number'])[7]")
	public WebElement BP_ac;
	@FindBy(xpath = "//b[contains(.,'Overage recorded of')]")
	public WebElement Overpayment_verification;
	@FindBy(xpath = "//a[contains(text(),'Receipt#')]")
	public WebElement UC_BlancePendingLink;
	@FindBy(xpath = "//*[@id=\"newQRCtrl\"]//table[@class='payments_received_container']/tbody/tr[@class='header_receipt_row']/td/a")
	public WebElement GlobalReceipts;
	public UC_BP_Trans_Bal_Check_AfterFullPayment_TC_24() {

		PageFactory.initElements(driver, this);
	}
	public void UC_BP_PayingWithBalancePending_TC_24(
			String BillDueAmount,String paidReceipts) throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		log.info("Account selected for payment is =" + accountnum.getAttribute("value"));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(Tdue));
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		System.out.println(due);
		wait.until(ExpectedConditions.elementToBeClickable(Amount));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//@@
		try {
			js.executeScript("arguments[0].scrollIntoView(true);", Amount);
			
		} catch (Exception e) {
			// TODO: handle exception
			Thread.sleep(5000);
			
		}
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		PM.click();
		Select drp = new Select(PM);
		drp.selectByVisibleText("BALANCE PENDING");
		Amount.clear();
		Amount.sendKeys(due);
		wait.until(ExpectedConditions.elementToBeClickable(BP_ac));
		BP_ac.click();
		Select dr = new Select(BP_ac);
		dr.selectByVisibleText("DEPOSITS-BALANCE PENDING");
		wait.until(ExpectedConditions.elementToBeClickable(BP));
		BP.click();
		BP.sendKeys(paidReceipts);
		BP.click();
		BP.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.elementToBeClickable(UC_BlancePendingLink));
		UC_BlancePendingLink.click();
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		F10.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(GlobalReceipts));
        //@@@@
		BillNum_Link.click();
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[14]"));
		System.out.println(l.getText());
		String bi = "0.00";
		String BillBalance = l.getText();
		System.out.println(BillBalance);

		if (BillBalance.contains(bi)) {
			log.info("Passed-Bill balance is Zero");

		} else {
			log.info("Failed-Bill balance is Not Zero");

			Assert.fail("Balance is not Zero");
		}

	}
		
		
		
		
	}


