package Module.UniversalCashiering;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Helper.GenerateRandom;
import TestBase.eTestBase;

public class PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15 extends eTestBase {
	public JavascriptExecutor js;
	public String Bill_Yp;
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(xpath = "//label[contains(.,'Bill Type')]")
	public WebElement billtype;
	@FindBy(xpath = "//span[contains(@id,'select2-status_flag-container')]")
	public WebElement flag;
	@FindBy(xpath = "//input[contains(@class,'select2-search__field')]")
	public WebElement searchFields;
	@FindBy(xpath = "//span[contains(@title,'NA-DO NOT ADVERTISE')]")
	public WebElement DonotAdvertise;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
	@FindBy(xpath = "//input[contains(@id,'enter_quickreceipt_button')]")
	public WebElement UC;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;
	@FindBy(xpath = "(//input[contains(@ng-model,'bill.amount_to_pay.total')])[1]")
	public WebElement forPartiaLPayment;
	@FindBy(xpath = "//span[contains(@id,'due')]")
	public WebElement Tdue;
	@FindBy(xpath = "//input[contains(@id,'payment_amount_0')]")
	public WebElement Amount;
	@FindBy(xpath = "//div[@ng-show=\"payment.method == 'CHECK'\"]//input[@type='text']")
	public WebElement check;
	@FindBy(xpath = "(//input[@ng-model='payment.check_number'])[2]")
	WebElement checkid2;
	@FindBy(xpath = "//input[contains(@id,'payment_name_0')]")
	public WebElement name;
	@FindBy(xpath = "(//input[@value='(F10) Finish Receipt'])[2]")
	public WebElement saveButton;
	@FindBy(xpath = "//h1[contains(.,'Receipt Processed Successfully')]")
	public WebElement Partial_verification;
	@FindBy(xpath = "(//select[@ng-model='payment.method_key'])")
	public WebElement paymentMethod1;
	@FindBy(xpath = "//h2[@class='tab'][contains(.,'Transactions')]")
	public WebElement Transaction;
	@FindBy(xpath = "(//b[contains(.,'P & I')])[2]")
	public WebElement toclick;
	@FindBy(xpath = "//div[@class='action-bar-grey']" + "//a[contains(text(),'Universal Cashiering')]")
	public WebElement UCSearch;

	public PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15() {

		PageFactory.initElements(driver, this);
	}

	@SuppressWarnings("static-access")
	public void PropertyTax_PartialPaymentMethod(
			String BillDueAmount, String PaymentMethod, String PartialPayment) throws InterruptedException {

		
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		List<WebElement> col = table.get(new GenerateRandom().R_num()).findElements(By.tagName("td"));
//		// @SuppressWarnings("unlikely-arg-type")
//		@SuppressWarnings("unlikely-arg-type")
//		String Bill_Amount = col.get(14).getText();
//		// Float Billtype=Float.valueOf(col.get(2).getText());
//		driver.findElement(By.xpath("//table[@id='data']/tbody/tr[3]/td[3]")).click();
//		// col.get(3).click();
		wait.until(ExpectedConditions.elementToBeClickable(UC));
		UC.click();
		wait.until(ExpectedConditions.visibilityOf(forPartiaLPayment));
		// @@@@@@@@@@@UC@@@@@@@@@@@@@@@@@@
		try {

			forPartiaLPayment.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		} catch (Exception e) {

		}
		try {
			// @@if it is single Bill
			if (driver.findElement(By.xpath(
					"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
					.isDisplayed()) {
				driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
						.clear();
				driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"))
						.sendKeys(PartialPayment);
				Bill_Yp = driver.findElement(By.xpath(
						"//div[@ng-show='bill.amount_to_pay && bill.universal_cashier != true']//input[@type='text']"
								+ "//parent::div//preceding-sibling::div//input[@id='quick_search']"))
						.getAttribute("value");
			}
		} catch (Exception e) {
			// @@if it is MultiYears Bill

			driver.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']")).clear();
			driver.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']"))
					.sendKeys(PartialPayment);
			Bill_Yp = driver
					.findElement(By.xpath("//input[@class='ng-pristine ng-valid ng-not-empty ng-touched']"
							+ "//parent::div//preceding-sibling::div//input[@id='quick_search']"))
					.getAttribute("value");
			log.info("Catch Block Executed");
		}
		// forPartiaLPayment.sendKeys("20");

		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", saveButton);
		Amount.click();
		Select ln = new Select((paymentMethod1));
		ln.selectByVisibleText(PaymentMethod);
		String due = Tdue.getText();
		due = due.replaceAll("[$,]", "");
		Amount.sendKeys(String.valueOf(Double.valueOf(due)));
		try {
			check.sendKeys("09");
		} catch (Exception e) {
			checkid2.sendKeys("09");
		}
		name.click();
		name.sendKeys("Jabed Liton");
		saveButton.click();
		try {
			driver.switchTo().alert().sendKeys("YES");
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		wait.until(ExpectedConditions.visibilityOf(Partial_verification));
		List<WebElement> ele_SingleBill = driver
				.findElements(By.xpath("//table[@class='payments_received_container']//tbody//tr"));
		int row = 0;
		int index = 1;
		for (row = 1; row < table.size(); row++) {
			List<WebElement> col_singleBill = ele_SingleBill.get(row).findElements(By.tagName("td"));
			Boolean li = col_singleBill.get(index).getText().trim().contains(Bill_Yp);

			if (li) {
				log.info("Inside the loop under global Received . " + "System should click on link = " + ""
						+ col_singleBill.get(index).getText());
				col_singleBill.get(index).click();
				break;

			}
		}
		// @@Transaction Tab checking for Partial Payment
		wait.until(ExpectedConditions.elementToBeClickable(Transaction));
		Transaction.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement l = driver.findElement(By.xpath("//*[@id='transactions']//*[@id='data']/tbody/tr[last()-5]/td[13]"));
		String BillBalance = l.getText();
		if (BillBalance.contains(PartialPayment)) {
			log.info("Passed-Bill is Partially paid and Recorded accordingly");

		} else {
			log.info("Failed partial payment missMatch");

			Assert.fail("Failed partial payment missMatch");
		}

	}
}
