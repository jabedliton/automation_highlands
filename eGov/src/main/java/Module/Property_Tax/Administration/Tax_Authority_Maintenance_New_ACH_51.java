package Module.Property_Tax.Administration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.google.gson.annotations.Until;

import TestBase.eTestBase;

public class Tax_Authority_Maintenance_New_ACH_51 extends eTestBase {
	public Actions ac;
	public JavascriptExecutor js;
	public Select sc;
	@FindBy(xpath = "//a[@href='#'][contains(.,'Property Tax')]")
	public WebElement PPT;
	@FindBy(xpath = "//a[@href='mod.php?mod=propertytax&mode=tax_authority_maintenance_new'][contains(.,'Tax Authority Maintenance New')]")
	public WebElement PPT_new;
	@FindBy(xpath = "(//ul[contains(@class,'select2-selection__rendered')])[1]")
	public WebElement TaxYear;
	@FindBy(xpath = "//li[contains(@class,'option--highlighted')]")
	public WebElement CurrentYear;
	@FindBy(xpath = "//button[@type='submit'][contains(.,'Search')]")
	public WebElement Search;
	@FindBy(xpath = "(//a[@href='javascript:void(0);'][contains(.,'Edit')])[1]")
	public WebElement First_Edit;
	@FindBy(xpath = "//select[contains(@id,'pay_method')]")
	public WebElement PM;
	@FindBy(xpath = "//select[contains(@id,'deposit_breakout')]")
	public WebElement D_Breakout;
	@FindBy(xpath = "//input[contains(@id,'ach_routing_number')]")
	public WebElement R_num;
	@FindBy(xpath = "//input[contains(@id,'ach_accounting_number')]")
	public WebElement ACC_num;
	@FindBy(xpath = "//span[@class='select2-selection__rendered'][contains(@id,'to-container')][contains(.,'COUNTY - TOT MILL')]")
	public WebElement Payable_To;
	@FindBy(xpath = "(//input[@type='search'])[3]")
	public WebElement Payable_To_search;
	@FindBy(xpath = "//input[@id='code']")
	public WebElement code;
	@FindBy(xpath = "//li[contains(@class,'option--highlighted')]")
	public WebElement High;
	@FindBy(xpath = "//input[contains(@type,'submit')]")
	public WebElement Save;
	@FindBy(xpath = "//img[@src='images/eGov-Shadow.png']")
	public WebElement scrollTo;
	@FindBy(xpath = "//button[@class='confirm'][contains(.,'OK')]")
	public WebElement OK;

	public Tax_Authority_Maintenance_New_ACH_51() {

		PageFactory.initElements(driver, this);

	}

	public void Tax_Aut_Maintenance_New_ACH(String P_Method,String Single_Deposit,String R_numb,String ACC,String  PayableTosearch,String codenum) throws InterruptedException {
		driver.navigate().refresh();
		ac = new Actions(driver);
		ac.moveToElement(PPT).perform();
		wait.until(ExpectedConditions.elementToBeClickable(PPT_new));
		PPT_new.click();
		wait.until(ExpectedConditions.elementToBeClickable(TaxYear));

		TaxYear.click();
		CurrentYear.click();
		//driver.findElement(By.xpath("(//li[contains(.,'2021')])[3]")).click();

		Search.click();
		wait.until(ExpectedConditions.elementToBeClickable(First_Edit));
		First_Edit.click();
		js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		PM.click();
		sc = new Select(PM);
		sc.selectByVisibleText(P_Method);
		sc = new Select(D_Breakout);
		sc.selectByVisibleText(Single_Deposit);
		R_num.sendKeys(R_numb);
		ACC_num.sendKeys(ACC);
		Payable_To.click();
		Payable_To_search.click();
		Payable_To_search.sendKeys(PayableTosearch);
		code.clear();
		code.sendKeys(codenum);
		High.click();
		js.executeScript("arguments[0].scrollIntoView();", scrollTo); 
		wait.until(ExpectedConditions.visibilityOf(Save));
		Save.click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class=\"sweet-alert showSweetAlert visible\"]"))));
        OK.click();
        js.executeScript("window.scrollBy(0,300)");
	}

}
