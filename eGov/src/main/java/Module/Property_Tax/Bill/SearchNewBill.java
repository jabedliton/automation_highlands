package Module.Property_Tax.Bill;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import TestBase.eTestBase;

public class SearchNewBill extends eTestBase {
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Search Bills NEW')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//span[contains(@id,'select2-tax_year-container')]")
	public WebElement EnterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement FilterTaxYear;
	@FindBy(xpath = ("//input[contains(@class,'select2-search__field')]"))
	public WebElement SelectTaxYear;
	@FindBy(xpath = ("//select[contains(@id,'balance_due_comparator')]"))
	public WebElement OperatorSignForbalance;
	@FindBy(id = ("balance_due"))
	public WebElement BalanceAmount;
	@FindBy(id = ("search_button"))
	public WebElement ClickOnSearch;
	@FindBy(xpath = "//th[contains(.,'Balance Due')]")
	public WebElement BalanceDueColumn;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;

	public SearchNewBill() {

		PageFactory.initElements(driver, this);
	}

	public void searchByBillNumber(String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount) throws InterruptedException, IOException {
		Actions actions = new Actions(driver);
		Thread.sleep(2000);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		wait.until(ExpectedConditions.elementToBeClickable(
				driver.findElement(By.xpath("//span[contains(@id,'select2-tax_year-container')]"))));
		EnterTaxYear.click();
		Thread.sleep(3000);
		FilterTaxYear.sendKeys(SearchYear);
		SelectTaxYear.click();
		OperatorSignForbalance.sendKeys(OperatorSign_Forbalance);
		BalanceAmount.sendKeys(BillDueAmount);
		ClickOnSearch.click();
		wait.until(ExpectedConditions.visibilityOf(BalanceDueColumn));
		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		int row = 0;
		int index = 6;
		for (row = 0; row < table.size(); row++) {
			List<WebElement> col = table.get(row).findElements(By.tagName("td"));
			// @SuppressWarnings("unlikely-arg-type")
			@SuppressWarnings("unlikely-arg-type")
			String Bill_Amount = col.get(index).getText();
			// System.out.println(Bill_Amount);
			if (!Bill_Amount.trim().contains(String.valueOf(Dueamount))) {
				Assert.assertNotEquals(Double.valueOf(Dueamount), Dueamount);
			}else {
				//System.out.println(Bill_Amount.trim().contains(String.valueOf(Dueamount))+"and matched the amount");
			}

		}
		
		System.out.println("Size of row count in table " + table.size());
	}

}
