package Module.Property_Tax.Bill;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import BasePages.LogInPage;

public class Bill_InformationPage extends LogInPage {

	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(text(),'Bill Information')]")
	public WebElement ClickOnSearchBillNew;
	@FindBy(xpath = "//input[@id='bill_number']")
	public WebElement BillNumberField;
	@FindBy(xpath = ("//input[@id='search_btn']"))
	public WebElement SearchButtonToClick;
	@FindBy(xpath = ("//div[@class='success message']"))
	public WebElement successMessage;
//WebTable
	@FindBy(xpath = ("//table[@id='data']/tbody/tr"))
	public WebElement WebTable;

	public Bill_InformationPage() {

		PageFactory.initElements(driver, this);
	}

	public void searchByBillNumber(String num) throws InterruptedException, IOException {
		Actions actions = new Actions(driver);
		Thread.sleep(2000);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(ClickOnSearchBillNew));
		ClickOnSearchBillNew.click();
		Thread.sleep(2000);
		BillNumberField.sendKeys(num);
		Thread.sleep(3000);
		SearchButtonToClick.click();

		sA.assertEquals(successMessage.isDisplayed(), true, "No Record found error ");

		List<WebElement> table = driver.findElements(By.xpath("//table[@id='data']/tbody/tr"));
		int row = 0;
		int index = 1;
		for (row = 0; row < table.size(); row++) {
			List<WebElement> col = table.get(row).findElements(By.tagName("td"));
			//@SuppressWarnings("unlikely-arg-type")
			Boolean Bill_Num_Match = col.get(index).getText().trim().equals(num);
			if (!Bill_Num_Match) {

				Assert.assertNotEquals(Bill_Num_Match, num);

			}

		}
		log.info("Size of row count in table " + table.size());
	}

}
