package Module.Property_Tax.Bill;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import TestBase.eTestBase;

public class CreateBillPPT extends eTestBase {
	@FindBy(xpath = "//a[contains(text(),'Property Tax')]")
	public WebElement ClickOnPropertyTax;
	@FindBy(xpath = "//a[contains(.,'Create Bill')]")
	public WebElement createBill;
	@FindBy(xpath = "//input[@class='adjustment-hidden ng-pristine ng-untouched ng-valid ng-empty box']")
	public WebElement Strap;
    
	public CreateBillPPT() {

		PageFactory.initElements(driver, this);
	}
 @Test
	public void CreatingBill()throws InterruptedException, IOException {
		Actions actions = new Actions(driver);
		Thread.sleep(2000);
		actions.moveToElement(ClickOnPropertyTax).perform();
		wait.until(ExpectedConditions.elementToBeClickable(createBill));
		createBill.click();
		Thread.sleep(2000);
		Strap.click();
}
}
