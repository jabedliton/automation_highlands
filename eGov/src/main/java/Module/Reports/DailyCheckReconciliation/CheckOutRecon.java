/**
 * 
 */
package Module.Reports.DailyCheckReconciliation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

/**
 * @author JabedLiton
 *
 */
public class CheckOutRecon extends eTestBase {
// //table[@class='sortable resizable']/tbody/tr/td[2]
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public  WebElement ReceiptsMenu;
	@FindBy(xpath = "//a[contains(.,'Checkout Reconciliation')]")
	public WebElement dailyckre;
	@FindBy(xpath = "//select[@name='date_range'][contains(@id,'range')]")
	public WebElement Date_Range;
	@FindBy(xpath = "//input[contains(@type,'submit')]")
	public WebElement Search;
	@FindBy(xpath = "(//div[contains(@class,'tableheader')])[1]")
	public WebElement Checkpoint;
	public CheckOutRecon() {

		PageFactory.initElements(driver, this);
	}

	public  void DailyCheckRe () throws InterruptedException {
    wait.until(ExpectedConditions.elementToBeClickable(ReceiptsMenu));
	Actions actions = new Actions(driver);
	actions.moveToElement(ReceiptsMenu).build().perform();
	 wait.until(ExpectedConditions.elementToBeClickable(dailyckre));
	dailyckre.click();
	Thread.sleep(2000);
	Date_Range.click();
	Select s=new Select(Date_Range);
	s.selectByIndex(1);
	Search.click();
	wait.until(ExpectedConditions.visibilityOf(Checkpoint));
	
}
	




}
