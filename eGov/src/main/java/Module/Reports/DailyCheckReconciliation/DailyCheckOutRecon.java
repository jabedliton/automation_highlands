/**
 * 
 */
package Module.Reports.DailyCheckReconciliation;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import TestBase.eTestBase;

/**
 * @author JabedLiton
 *
 */
public class DailyCheckOutRecon extends eTestBase {
// //table[@class='sortable resizable']/tbody/tr/td[2]
	@FindBy(xpath = "//a[@href='#'][contains(.,'Receipts')]")
	public WebElement ReceiptsMenu;
	@FindBy(xpath = "//a[contains(.,'Daily Check Reconciliation')]")
	public WebElement dailyckre;
	@FindBy(css = "#work_date")
	public WebElement WorkDate;// Date for Search
	@FindBy(css = "#user_list")
	public WebElement Receipt_User;
	@FindBy(css = "#run")
	public WebElement FindCheck;
	@FindBy(css = "input#amount.text")
	public WebElement CheckMatch;
	@FindBy(css = "#continue")
	public WebElement Continue;
	@FindBy(xpath = "//input[@value='Complete Checkout']")
	public WebElement CompleteCheckout;
	@FindBy(xpath = "//tbody/tr[7]/td[1]/input[2]")
	public WebElement myDeposit;
	@FindBy(xpath = "//body/div[@id='outer']/div[@id='page']/div[@id='content']/div[@id='dailyCheckoutReportContainer']"
			+ "/form[1]/div[1]/div[1]/div[2]/div[2]")
	public WebElement CKpoint;
	@FindBy(xpath = "//strong[contains(text(),'If you close your work date with a shortage an adj')]")
	public WebElement ShortMessage;
	String timestamp = new SimpleDateFormat("MM/dd/yyyy").format(new Date());

	public DailyCheckOutRecon() {

		PageFactory.initElements(driver, this);
	}

	public void DailyCheckRe(String User) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(ReceiptsMenu));
		Actions actions = new Actions(driver);
		actions.moveToElement(ReceiptsMenu).build().perform();
		wait.until(ExpectedConditions.elementToBeClickable(dailyckre));
		dailyckre.click();
		Thread.sleep(2000);
		WorkDate.clear();
		WorkDate.sendKeys(timestamp);
		Receipt_User.sendKeys(User);
		FindCheck.click();
		Thread.sleep(7000);
		String StringValue = CheckMatch.getAttribute("value");
		Boolean t = StringValue.contains("ALL CHECKS MATCHED PRESS F6 TO PROCEED");
		if (t == true) {

			Continue.click();
		} else {

			driver.quit();
		}
		wait.until(ExpectedConditions.elementToBeClickable(CompleteCheckout));
		CompleteCheckout.click();
		try {
			wait.until(ExpectedConditions.visibilityOf(CKpoint));

		} catch (Exception e) {
			// TODO: handle exception
		}
		myDeposit.click();
		ShortMessage.isDisplayed();
		//// strong[contains(text(),'If you close your work date with a shortage an
		//// adj')]
//	s.selectByIndex(1);
//	Search.click();
//	wait.until(ExpectedConditions.visibilityOf(Checkpoint));
//	
	}

}
