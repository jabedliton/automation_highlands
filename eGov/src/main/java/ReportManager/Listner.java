package ReportManager;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import BasePages.DataconstantClass;
import TestBase.eTestBase;
import UtilPackage.ExcelResult;
import UtilPackage.ScreenShot;

public class Listner extends eTestBase implements ITestListener {
	// String times = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new
	// Date());
	ScreenShot Sshot = new ScreenShot();
	public static Logger log = Logger.getLogger("TestInfo");
	public void onTestFailure(ITestResult re) {
		try {
			Sshot.failed_TCs(re.getName() + "_" + times);
			// System.out.println(re.getName());
			ExcelResult.updateResult(DataconstantClass.excellocation, prop.getProperty("TestResult"), re.getName(),
					"Failed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	public void onTestStart(ITestResult re) {

		log.info("Execution started for :" + re.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult re) {
		try {
			Sshot.passed_TCs(re.getName() + "_" + times);
			// System.out.println(re.getName());
			ExcelResult.updateResult(DataconstantClass.excellocation, prop.getProperty("TestResult"), re.getName(),
					prop.getProperty("PassedRessult"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void onFinish(ITestContext a) {
		
		// TODO Auto-generated method stub
			log.info("Calling onFinish for method : ");
		   // driver.close();
	}

}
