package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PPTax_oneOverPayment_Check_with_Mis_Bal_Check_TC_19;
import Module.UniversalCashiering.PropertyTax_MultiPaymentMethod_Check_Cash_withMis_Bal_Check_TC_17;
import TestBase.eTestBase;

public class PPTax_oneOverPayment_Check_with_Mis_BalanceCheck_TC_19 extends eTestBase {

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();

		}

	}

	@Test
	public void Verify_PPTax_OverPayment_Check_with_Mis_BalanceCheck_TC_19() throws IOException, InterruptedException {
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		// @@@@@@
		SearchBill sb = new SearchBill();
		sb.searchingBill("2021", ">", "1000", 2);
		// @@@@
		new PPTax_oneOverPayment_Check_with_Mis_Bal_Check_TC_19().
		// @@Has Default Check with Cash
				PPTax_PaymentMethod_WithMis_overpayment("100", 5.00);
	}

	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
