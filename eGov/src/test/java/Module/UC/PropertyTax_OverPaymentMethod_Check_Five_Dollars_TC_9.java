package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_Bal__TC_9;
import TestBase.eTestBase;

public class PropertyTax_OverPaymentMethod_Check_Five_Dollars_TC_9 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_OverPaymentMethod_Check_5_over_BalCheck_TC_9() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_Bal__TC_9 m=new PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_Bal__TC_9();
		m.PropertyTax_OverPaymentMethod_5_dollars_TC_9( "200","CHECK",5.00);
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}