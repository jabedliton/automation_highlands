package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Helper.SearchBill_Nocert;
import Module.UniversalCashiering.PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15;
import TestBase.eTestBase;

public class PPTax_Partial_Payment_Bal_Check_TC_15 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();		
		}
		
	}
 @Test
	public void Verify_PropertyTax_PartialPaymentMethod_Check_Bal_Check_TC_15() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill sb=new SearchBill();
		sb.searchingBill("2021", ">", "500",2);
		PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15 m=new PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15();
		m.PropertyTax_PartialPaymentMethod("100","CHECK","20.00");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}