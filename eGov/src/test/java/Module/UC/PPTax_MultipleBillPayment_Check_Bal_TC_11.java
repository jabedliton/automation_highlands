 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import TestBase.eTestBase;

public class PPTax_MultipleBillPayment_Check_Bal_TC_11 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_MultipleBillPayment_Check_Zero_Balance_After_FullPayment_TC_11() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11 m=new PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11();
		m.PPTax_MultiPayment_CheckBalance("2021", ">", "1000");
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
