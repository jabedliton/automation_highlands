 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import Module.UniversalCashiering.PropertyTax_OnePayment_OverpaymentWorkflow__TC_25;
import TestBase.eTestBase;

public class PropertyTax_PaymentOverThresHold_WorkFlow_TC_25 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPTax_OnePaymentMethod_OverThreshold_TC_25() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		PropertyTax_OnePayment_OverpaymentWorkflow__TC_25 Opw=new 
				PropertyTax_OnePayment_OverpaymentWorkflow__TC_25 ();
		Opw.PropertyTax_OnePaymentMethod_WorkFlow("200", 99.00);
		
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
