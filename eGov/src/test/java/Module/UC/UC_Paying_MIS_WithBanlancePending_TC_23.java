package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Module.UniversalCashiering.UC_BP_PayingWith_BP_tc_23;
import TestBase.eTestBase;

public class UC_Paying_MIS_WithBanlancePending_TC_23 extends eTestBase {

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}

	}

	@Test
	public void VerifyThat_PayingWithBanlancePending_TC_23() throws InterruptedException, IOException {
		UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22 m=new 
		UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22();
		m.Verify_BP_Added_EntryCheck_TC_22();
		log.info("Paid Receipts number is collected ="+m.PaidReceipts_Link);
		// @
		new UC_BP_PayingWith_BP_tc_23().UC_PayingWithBalancePendins_TC_23(m.PaidReceipts_Link,"5");

	}
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
