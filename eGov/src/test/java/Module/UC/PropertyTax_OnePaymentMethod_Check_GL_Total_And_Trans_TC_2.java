 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Check_GL_Total_And_Trans_TC_2 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_FullPyments_PropertyTax_OnePaymentMethod_Check_GL_For_Total_And_Trans_TC_2() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		
		
		PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2 m=new PropertyTax_OnePaymentMethod_Default_Check_GL_TC_2();
		m.PropertyTax_OnePaymentMethod_GL_Total_Trans("200");
	}
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
