package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.PPTax_Partial_PaidBill_Paying_InFull_Bal_Check_TC_34;
import Module.UniversalCashiering.PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15;
import TestBase.eTestBase;

public class PPTax_PayingPartial_PaidBillInfull_Bal_Check_TC_34 extends eTestBase {

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}
	}

	@Test
	public void Verify_Partial_PaidBill_Paying_InFull_Bal_Check_Bal_Check_TC_34()
			throws IOException, InterruptedException {
		// @@@@PayingBill Partially@@@@@@@@@
		PPTax_Partial_Payment_Bal_Check_TC_15 P_pay = new PPTax_Partial_Payment_Bal_Check_TC_15();
		P_pay.Verify_PropertyTax_PartialPaymentMethod_Check_Bal_Check_TC_15();
		// @@@@Paying that bill in full@@@@@@@@@
		PPTax_Partial_PaidBill_Paying_InFull_Bal_Check_TC_34 m = new PPTax_Partial_PaidBill_Paying_InFull_Bal_Check_TC_34();
		m.Partial_PaidBill_Paying_InFull_Bal_Check_TC_34("CHECK");
	}
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
}