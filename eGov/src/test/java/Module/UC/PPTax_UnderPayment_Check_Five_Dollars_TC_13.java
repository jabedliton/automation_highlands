package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PPTax_UnderPayment_Five_dollars_Bal_Check_TC_13;
import TestBase.eTestBase;

public class PPTax_UnderPayment_Check_Five_Dollars_TC_13 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_UnderPaymentMethod_Check_5_Dollars_TC_13() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		PPTax_UnderPayment_Five_dollars_Bal_Check_TC_13 m=new PPTax_UnderPayment_Five_dollars_Bal_Check_TC_13();
		m.PPTax_underPaymentMethod_5_dollars("200","CHECK");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}