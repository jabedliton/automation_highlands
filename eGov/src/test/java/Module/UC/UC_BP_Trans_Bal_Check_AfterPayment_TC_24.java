package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helper.SearchBill;
import Module.UniversalCashiering.UC_BP_Trans_Bal_Check_AfterFullPayment_TC_24;
import TestBase.eTestBase;

public class UC_BP_Trans_Bal_Check_AfterPayment_TC_24 extends eTestBase {

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}
	}
	@Test
	public void Verify_UC_BP_Trans_Bal_Check_AfterPayment_TC_24() throws InterruptedException, IOException {
		UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22 T=new 
				UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22();
		T.Verify_BP_Added_EntryCheck_TC_22();
		// @
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		
		
	new UC_BP_Trans_Bal_Check_AfterFullPayment_TC_24().
	UC_BP_PayingWithBalancePending_TC_24("200",T.PaidReceipts_Link);
	}

	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
