package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.UC_Balance_Pending_Mis_GLCheck_TC_21;
import TestBase.eTestBase;

public class UC_Balance_Pending_Entry_GL_TC_21 extends eTestBase {
  public String Ref_ID;
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}

	}

	@Test
	public void Verify_UC_Balance_Pending_GLTC_21() throws InterruptedException, IOException {
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		// @Passing amount, ckNum , Name for the Check payment
		UC_Balance_Pending_Mis_GLCheck_TC_21 TC=new UC_Balance_Pending_Mis_GLCheck_TC_21();
		TC.UC_BPending_Mis_Bal_TC_21("100000", "109", "Jabed");
		Ref_ID=TC.RefNumber;
		log.info("Ref_ID collected for Ref is =" + Ref_ID);
	}

	@AfterMethod
	public void teardown() {
		
		// driver.quit();
	}

}
