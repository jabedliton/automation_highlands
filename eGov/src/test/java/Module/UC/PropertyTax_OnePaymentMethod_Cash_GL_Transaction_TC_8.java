 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Cash_BAL_zero_UnderTrnsactionTab_TC_7;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Cash_GL_Total_Trans_Debit_Credit_Balance_TC_8;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Cash_GL_Transaction_TC_8 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPTax_OnePaymentMethod_Cash_GL_Trans_Debit_Credit_TC_8() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OnePaymentMethod_Cash_GL_Total_Trans_Debit_Credit_Balance_TC_8 m=new PropertyTax_OnePaymentMethod_Cash_GL_Total_Trans_Debit_Credit_Balance_TC_8();
		m.PropertyTax_OnePaymentMethod_Cash_("200");
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
