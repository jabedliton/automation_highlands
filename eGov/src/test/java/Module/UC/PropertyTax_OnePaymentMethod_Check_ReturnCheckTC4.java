 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_ReturnCheck_TC_4;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Check_ReturnCheckTC4 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();		
		}
		
	}
 @Test
	public void Verify_PPTax_OnePaymentMethod_ReturnCheck_BalCheck_TC_4() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		SearchBill s = new SearchBill();
		s.searchingBill("2021", "=", "0", 2);
		
		PropertyTax_OnePaymentMethod_Default_Check_ReturnCheck_TC_4 m=new PropertyTax_OnePaymentMethod_Default_Check_ReturnCheck_TC_4();
		m.PropertyTax_OnePaymentMethod_ReturnCheck();
	}
	
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
