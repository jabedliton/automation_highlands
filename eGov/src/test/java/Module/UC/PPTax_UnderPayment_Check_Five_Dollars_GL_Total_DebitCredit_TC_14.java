package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PPTax_UnderPayment_Five_dollars_GL_Total_Debit_Credit_TC_14;
import TestBase.eTestBase;

public class PPTax_UnderPayment_Check_Five_Dollars_GL_Total_DebitCredit_TC_14 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPTax_UnderPayment_Check_5_Dollars_GL_Total_DebitCredit_TC_14() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		PPTax_UnderPayment_Five_dollars_GL_Total_Debit_Credit_TC_14 m=new PPTax_UnderPayment_Five_dollars_GL_Total_Debit_Credit_TC_14();
		m.PPTax_underPaymentMethod_5_dollars("200","CHECK");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}