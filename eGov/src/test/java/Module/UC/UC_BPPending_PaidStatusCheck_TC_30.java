package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Module.UniversalCashiering.UC_BP_Added_EntryCheck_TC_22;
import Module.UniversalCashiering.UC_BP_Trans_Paid_StatusCheck_Check_TC_30;
import TestBase.eTestBase;

public class UC_BPPending_PaidStatusCheck_TC_30 extends eTestBase {
  public String PaidReceipts_Link;
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}

	}

	@Test
	public void Verify_BP_PaidStatusCheck_TC_30() throws InterruptedException, IOException {
		UC_Balance_Pending_Entry_GL_TC_21 m= new UC_Balance_Pending_Entry_GL_TC_21();
		m.Verify_UC_Balance_Pending_GLTC_21();
		UC_BP_Added_EntryCheck_TC_22 tc= new UC_BP_Added_EntryCheck_TC_22();
		tc.UC_BalancePendingAddedEntryCheck_TC_22(m.Ref_ID);
		PaidReceipts_Link=tc.RefNumber_Link;
		log.info("Paid Receipts number for Balance Pending is = "+PaidReceipts_Link);
		//@@@@@@@@@@@@@@@@@@
		
		UC_BP_Trans_Paid_StatusCheck_Check_TC_30 BP=new UC_BP_Trans_Paid_StatusCheck_Check_TC_30();
		BP.UC_BP_PaidStatusCheck_TC_30("PAID");
	}
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
