package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.PPTax_MultipleBillPayment__Check_BAL_zero_UnderTrnsactionTab_TC_11;
import Module.UniversalCashiering.PPTax_MultipleBillPayment__Check_GL_TransDebitCreditMatch_TC_12;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import TestBase.eTestBase;

public class PPTax_MultipleBillPayment_Check_GL_Total_Debit_Credit_Match_TC_12 extends eTestBase {

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();

		}

	}

	@Test
	public void Verify_PPTax_MultipleBillPayment_GL_Total_GL_DebitCredit_TC_12()
			throws IOException, InterruptedException {

		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		PPTax_MultipleBillPayment__Check_GL_TransDebitCreditMatch_TC_12 m = new PPTax_MultipleBillPayment__Check_GL_TransDebitCreditMatch_TC_12();
		m.PPTax_MultipleBillPayment__Check_GL_TransDebitCreditMatch("2021", ">", "1000");
	}

	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
