 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Check_Bal_TC_1 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();	
		}
		
	}
 @Test
	public void Verify_PropertyTax_OnePaymentMethod_Check_Zero_Balance_After_FullPayment_TC_1() throws IOException, InterruptedException {
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "10000", 2);
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1 m=new PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1();
		m.PropertyTax_OnePaymentMethod_CheckBalance( "200");
	}
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}	
}
