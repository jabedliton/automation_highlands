package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_PostMarking_TC_3;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Check_Default_PostMarking_TC_3 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_OnePaymentMethod_Check_PostMarking_TC_3() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "5000", 2);
		
		
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OnePaymentMethod_Default_Check_PostMarking_TC_3 m=new PropertyTax_OnePaymentMethod_Default_Check_PostMarking_TC_3();
		m.PropertyTax_OnePaymentMethod_CheckPostmarking("200","2021-07-31");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
