package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.PropertyTax_Partial_PaymentMethod_Bal_Check_TC_15;
import Module.UniversalCashiering.PropertyTax_Partial_PaymentMethod_GLR_Total_Debit_Credit_Check_TC_16;
import TestBase.eTestBase;

public class PPTax_Partial_Payment_GLR_Total_Debit_Credit_Check_TC_16 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();		
		}
		
	}
 @Test
	public void Verify_PropertyTax_PartialPayment_GLR_Total_Debit_Credit_Check_TC_16() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		PropertyTax_Partial_PaymentMethod_GLR_Total_Debit_Credit_Check_TC_16 m=new PropertyTax_Partial_PaymentMethod_GLR_Total_Debit_Credit_Check_TC_16();
		m.PropertyTax_PartialPaymentMethod("2021", ">", "10000","CHECK","20.00");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}