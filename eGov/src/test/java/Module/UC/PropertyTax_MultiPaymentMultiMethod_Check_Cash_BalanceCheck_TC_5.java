package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_MultiPaymentMethod_Check_Cash_Transction_Bal_Check_TC_5;
import TestBase.eTestBase;

public class PropertyTax_MultiPaymentMultiMethod_Check_Cash_BalanceCheck_TC_5 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPT_MultiPaymentMethod_Check_Cash_Trans_Tab_TC_5() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		new PropertyTax_MultiPaymentMethod_Check_Cash_Transction_Bal_Check_TC_5().
		PropertyTax_MultiPaymentMethod_Default("200", "CHECK", "CASH");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
