package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_GL__TC_10;
import TestBase.eTestBase;

public class PropertyTax_OverPaymentMethod_Check_Five_Dollars_GL_TC_10 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_OverPaymentMethod_Check_5_over_GLCheck_TC_10() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		
		
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_GL__TC_10 m=new PropertyTax_OverPaymentMethod_Default_Check_Of_Five_dollars_GL__TC_10();
		m.PropertyTax_OverPaymentMethod_5_dollars_GL_TC_10("200","CHECK",5.00);
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}