 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PPTax_OnePayment_OP_AmountVerification_TC_29;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import Module.UniversalCashiering.PropertyTax_OnePayment_OverpaymentWorkflow__TC_25;
import TestBase.eTestBase;

public class PPTax_OPTool_Amount_GLR_number_Check_Amount_TC_29 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPTax_OPTool_Amount_GLR_number_Check_Amount_TC_29() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//@@@@@@
				SearchBill sb=new SearchBill();
				sb.searchingBill("2021", ">", "1000",2);
		PPTax_OnePayment_OP_AmountVerification_TC_29 Opw=new 
				PPTax_OnePayment_OP_AmountVerification_TC_29 ();
		Opw.PropertyTax_OP_AmountVerification_TC_29("100", 99.00);	
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
