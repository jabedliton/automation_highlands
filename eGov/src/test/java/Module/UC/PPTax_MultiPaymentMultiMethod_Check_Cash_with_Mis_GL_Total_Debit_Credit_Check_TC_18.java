package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_MultiPaymentMethod_Check_Cash_withMis_GL_DebitCredit_TC_18;
import TestBase.eTestBase;

public class PPTax_MultiPaymentMultiMethod_Check_Cash_with_Mis_GL_Total_Debit_Credit_Check_TC_18 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_MultiPaymentMethod_Check_Cash_withMis_GL_Debit_Credit_TC_18() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//@@@@@@
		SearchBill sb=new SearchBill();
		sb.searchingBill("2021", ">", "1000",2);
		//@@@@
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		new PropertyTax_MultiPaymentMethod_Check_Cash_withMis_GL_DebitCredit_TC_18().
		PropertyTax_MultiPaymentMethod_Default("100", "CHECK", "CASH");
	}
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}	
}