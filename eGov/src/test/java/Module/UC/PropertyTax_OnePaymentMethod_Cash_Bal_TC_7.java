 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Cash_BAL_zero_UnderTrnsactionTab_TC_7;
import Module.UniversalCashiering.PropertyTax_OnePaymentMethod_Default_Check_BAL_zero_UnderTrnsactionTab_TC_1;
import TestBase.eTestBase;

public class PropertyTax_OnePaymentMethod_Cash_Bal_TC_7 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPTax_OnePaymentMethod_Cash_Zero_Balance_After_FullPayment_TC_7() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		PropertyTax_OnePaymentMethod_Cash_BAL_zero_UnderTrnsactionTab_TC_7 m=new PropertyTax_OnePaymentMethod_Cash_BAL_zero_UnderTrnsactionTab_TC_7();
		m.PropertyTax_OnePaymentMethod_Cash_("2021", ">", "10000");
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
