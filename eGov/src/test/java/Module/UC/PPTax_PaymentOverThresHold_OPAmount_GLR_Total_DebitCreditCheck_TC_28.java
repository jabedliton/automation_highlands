 package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_GLR_TC_28;
import Module.UniversalCashiering.PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_TC_26;
import TestBase.eTestBase;

public class PPTax_PaymentOverThresHold_OPAmount_GLR_Total_DebitCreditCheck_TC_28 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PropertyTax_PaymentOverThresHold_OPAmount_GLR_TC_28() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_GLR_TC_28 Opw=new 
				PropertyTax_OnePayment_OverpaymentWorkflow_OPAmount_GLR_TC_28 ();
		Opw.PPTax_OnePayment_Overpayment_OPAmount_GLR_Check_TC_28("200", 99.00);
		
		
		
	}
		
    @AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
