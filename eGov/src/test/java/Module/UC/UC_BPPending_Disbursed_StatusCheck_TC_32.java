package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Module.UniversalCashiering.UC_BP_PayingWith_BP_tc_23;
import Module.UniversalCashiering.UC_BP_Trans_Disbursment_StatusCheck_Check_TC_32;
import Module.UniversalCashiering.UC_BP_Trans_Partial_StatusCheck_Check_TC_31;
import TestBase.eTestBase;

public class UC_BPPending_Disbursed_StatusCheck_TC_32 extends eTestBase {
	public String PaidReceipts_Link;

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}

	}

	@Test
	public void Verify_BPPending_Disbursed_StatusCheck_TC_32() throws InterruptedException, IOException {
		UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22 m = new UC_OnePaymentMethod_BPPending_Added_EntryCheck_TC_22();
		m.Verify_BP_Added_EntryCheck_TC_22();
		log.info("Paid Receipts number is collected =" + m.PaidReceipts_Link);
		// @@@@@@Paying with BP@@@@@@@@@@@@@@@@@@@@@@@
		new UC_BP_PayingWith_BP_tc_23().UC_PayingWithBalancePendins_TC_23(m.PaidReceipts_Link, "5");
		// @@@@@@@@@@@@@@@@@@GettingIntoBp ModuleForPartial@@@@@@@@@@@@
		UC_BP_Trans_Partial_StatusCheck_Check_TC_31 BP = new UC_BP_Trans_Partial_StatusCheck_Check_TC_31();
		BP.UC_BP_PartialStatusCheck_TC_31(m.ID, "PARTIAL");
		//// @@@@@@@@@@@@@@@@@@GettingIntoBp Module For Disbursed@@@@@@@@@@@@
		UC_BP_Trans_Disbursment_StatusCheck_Check_TC_32 DS = new UC_BP_Trans_Disbursment_StatusCheck_Check_TC_32();
		DS.UC_BP_DisbursedStatusCheck_TC_32(BP.Remaining, m.PaidReceipts_Link, m.ID, "DISBURSED");

	}

	@AfterMethod
	public void teardown() {
		// driver.quit();
	}

}
