package Module.UC;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.SearchBill;
import Module.UniversalCashiering.PropertyTax_MultiPaymentMethod_Check_Cash_GL_Trans_Check_TC_6;
import TestBase.eTestBase;

public class PropertyTax_MultiPaymentMultiMethod_Check_Cash_GL_Trans_TC_6 extends eTestBase {
	
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
			
		}
		
	}
 @Test
	public void Verify_PPT_MultiPaymentMethod_Check_Cash_Trans_Tab_TC_6() throws IOException, InterruptedException {
		
		LoginToeGoV L = new LoginToeGoV();
		L.Verfiy_eGove_login();
		SearchBill s = new SearchBill();
		s.searchingBill("2021", ">", "1000", 2);
		//String SearchYear,String OperatorSign_Forbalance,String BillDueAmount,Double Dueamount
		new PropertyTax_MultiPaymentMethod_Check_Cash_GL_Trans_Check_TC_6().
		PropertyTax_MultiPaymentMethod_CheckCash("200", "CHECK", "CASH");
	}
	
	@AfterMethod
	public void teardown() {
		// driver.quit();
	}
	
}
