package Module.Checks.Freeform;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Freeform.Check_FreeForm_DomesticesCreateCheck_62;
import Module.Checks.Registration.Check_Reconciliation_ReportByDateRange_59;
import Module.Checks.Registration.Check_Reconciliation_ReportByDateRange_Reconciled_60;
import TestBase.eTestBase;

public class Check_FreeForm_Domest_CreateCheck_62 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
@Test
public void Verify_CheckFreeForm_Domestic_CreateCheck_62() throws IOException, InterruptedException {
		
		LoginToeGoV log=new LoginToeGoV();
		log.Verfiy_eGove_login();
		Check_FreeForm_DomesticesCreateCheck_62 re=new Check_FreeForm_DomesticesCreateCheck_62();
		re.CheckFreeForm_Domestic_CreateCheck_62("9999-51351", "1200", "12/12/2022", "REFUNDS");
	
	}
	
	
}
