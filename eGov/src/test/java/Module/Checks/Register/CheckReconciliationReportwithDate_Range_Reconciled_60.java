package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.Check_Reconciliation_ReportByDateRange_59;
import Module.Checks.Registration.Check_Reconciliation_ReportByDateRange_Reconciled_60;
import TestBase.eTestBase;

public class CheckReconciliationReportwithDate_Range_Reconciled_60 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
public void Verify_Check_Reconciliation_Report_with_Date_Range_Reconciled_60() throws IOException, InterruptedException {
		
		LoginToeGoV log=new LoginToeGoV();
		log.Verfiy_eGove_login();
		Check_Reconciliation_ReportByDateRange_Reconciled_60 re=new Check_Reconciliation_ReportByDateRange_Reconciled_60();
		re.Check_Reconciliation_Report_with_Date_Range_status_IncludeAll_61("YEAR TO DATE", "Reconciled");
	}
	
	
}
