package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.ListingCheck_54;
import TestBase.eTestBase;

public class SearchCheck_Bydate_54 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
	public void Verify_UserCanSearchCheckBydate_54() throws IOException, InterruptedException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		
		ListingCheck_54 cla=new ListingCheck_54();
		cla.Listing_Check_54("MONTH TO DATE");
	
	
	}
	
	
	
}
