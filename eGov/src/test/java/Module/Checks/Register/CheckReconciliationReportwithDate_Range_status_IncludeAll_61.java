package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.Check_Reconciliation_ReportByDateRange_Status_IncludeAll_61;
import TestBase.eTestBase;

public class CheckReconciliationReportwithDate_Range_status_IncludeAll_61 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
@Test
public void Verify_Check_Reconciliation_Report_with_Date_Range_Status_IncludeAll_61() throws IOException, InterruptedException {
		
		LoginToeGoV log=new LoginToeGoV();
		log.Verfiy_eGove_login();
		
		Check_Reconciliation_ReportByDateRange_Status_IncludeAll_61 b=new Check_Reconciliation_ReportByDateRange_Status_IncludeAll_61();
		b.CheckReconciliationReportByDateRangeStatusIncludeAll_61("YEAR TO DATE", "Include All");
	}
	
	
}
