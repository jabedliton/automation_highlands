package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.SearchCheckWith_Status_Under_AccounInfo_58;
import TestBase.eTestBase;

public class SearchCheckPerAccountInformation_Status_58 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
	public void Verify_UserCanSearchCheckWith_Status_Under_AccounInfo_58() throws IOException, InterruptedException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		
		SearchCheckWith_Status_Under_AccounInfo_58 n=new SearchCheckWith_Status_Under_AccounInfo_58();
		n.User_SearchCheckWith_Status_Under_AccounInfo_58("", "947", "947","PRINTED");	
	}
	
	
	
}
