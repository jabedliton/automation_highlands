package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.ListCheckwithCheckRangeinformation_56;
import Module.Checks.Registration.ListingCheck_54;
import TestBase.eTestBase;

public class SearchCheckwitrhCheckInformation_Range_56 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
	public void Verify_UserCanSearchCheck_withCheckInformation_56() throws IOException, InterruptedException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		
		ListCheckwithCheckRangeinformation_56 n=new ListCheckwithCheckRangeinformation_56();
		n.List_Check_withCheckInformation_56("YEAR TO DATE", "91072", "91076");
	
	
	}
	
	
	
}
