package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.ExportSearched_ListedCheckToCSV_55;
import Module.Checks.Registration.ListingCheck_54;
import TestBase.eTestBase;

public class ExportListedCheckToCSV_55 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
	public void Verify_UserCanExportListedCheckInfo_CSV_55() throws IOException, InterruptedException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		ExportSearched_ListedCheckToCSV_55 e=new ExportSearched_ListedCheckToCSV_55();
		e.ExportListed_Check_CSV_55("MONTH TO DATE");
	}
	
	
	
}
