package Module.Checks.Register;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Checks.Registration.ListCheckwithCheckRangeinformation_56;
import Module.Checks.Registration.ListingCheck_54;
import Module.Checks.Registration.Opening_ListedCheck_FromUI_57;
import TestBase.eTestBase;

public class OpenListed_CheckFrom_UI_57 extends eTestBase {

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}
	
	
	@Test
	public void Verify_UserCanOpenListed_CheckFrom_UI_57() throws IOException, InterruptedException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		
		Opening_ListedCheck_FromUI_57 n=new Opening_ListedCheck_FromUI_57();
		n.User_OpeningListedCheck_FromUI_57("YEAR TO DATE", "91072", "91076");
		
	
	
	}
	
	
	
}
