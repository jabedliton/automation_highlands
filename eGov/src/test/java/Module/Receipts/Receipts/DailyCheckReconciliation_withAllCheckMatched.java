package Module.Receipts.Receipts;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import Module.Reports.DailyCheckReconciliation.DailyCheckOutRecon;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class DailyCheckReconciliation_withAllCheckMatched extends eTestBase {
	public static LogInPage LP;
	public static String tc_name;
	public static String TC_name = "Verfiy_eGove_login";
	public static String Site_Url;

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}
  
	}

	@Test
	public static void Verify_DailyCheckReconciliationDepositShort() throws InterruptedException, IOException {
		// @@Collecting Url for Initialization Method
		ExcelReadTestData.Testdata_String("TestResult", TC_name, 3);
		Site_Url = ExcelReadTestData.Url;
		log.info("Url is collected and passing it to initialization Method");
		// @@launching eGoV
		initialization(Site_Url);
		LP = new LogInPage();
		LP.Login(prop.getProperty("Login_Un"), prop.getProperty("Login_Pw"));
		 tc_name = new Object() {
			}.getClass().getEnclosingMethod().getName();
			System.out.println(tc_name);
		  ExcelReadTestData.Testdata_String(prop.getProperty("TestResult"), 
						"Verify_DailyCheckReconciliationDepositShort",3);
		System.out.println(ExcelReadTestData.ce);
		new DailyCheckOutRecon().DailyCheckRe(ExcelReadTestData.ce);
	  

	}

	@AfterMethod
	public void teardown() {
   // driver.quit();
	}

}
