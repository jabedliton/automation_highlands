package Module.Receipts.AccountingReport;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import Module.AccountsReports.CheckOutRecon_42;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class CheckReconciliation_42 extends eTestBase {
	public static LogInPage LP;
	public static String TC_name = "Verfiy_eGove_login";
	public static String Site_Url;

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}

	}

	@Test
	public static void Verify_CheckoutReconciliation_42() throws InterruptedException, IOException {
		// @@Collecting Url for Initialization Method
		ExcelReadTestData.Testdata_String("TestResult", TC_name, 3);
		Site_Url = ExcelReadTestData.Url;
		log.info("Url is collected and passing it to initialization Method");
		// @@launching eGoV
		initialization(Site_Url);
		LP = new LogInPage();
		LP.Login(prop.getProperty("Login_Un"), prop.getProperty("Login_Pw"));
		new CheckOutRecon_42().DailyCheckRe_42();
	  

	}

	@AfterMethod
	public void teardown() {
   // driver.quit();
	}

}
