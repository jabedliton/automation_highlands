package Module.Receipts.AccountingReport;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.AccountsReports.Voided_Receipts_48;
import TestBase.eTestBase;

public class Voided_Receipts_Running_48 extends eTestBase {

	@BeforeMethod
	public void setUp() throws IOException {
		if (driver != null) {
			driver.close();

		}
	}

	
	@Test
	public void  Verify_Voided_Re_48() throws IOException, InterruptedException {
		LoginToeGoV b=new LoginToeGoV();
		b.Verfiy_eGove_login();
		//@@Date passing Yesterday
		Voided_Receipts_48 n=new Voided_Receipts_48();
		//@@QuickDate, Date Range by index 
		n.Voided_Receipts_TC_48("4","11/15/2020 - 11/15/2021");
		
	
	}
	
}
