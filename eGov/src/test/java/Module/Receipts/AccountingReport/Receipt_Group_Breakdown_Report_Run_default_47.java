package Module.Receipts.AccountingReport;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Helper.PDFreader;
import Module.AccountsReports.Receipt_Group_Breakdown_Report_47;
import TestBase.eTestBase;

public class Receipt_Group_Breakdown_Report_Run_default_47 extends eTestBase {

	@BeforeMethod
	public void setUp() throws IOException {
		if (driver != null) {
			driver.close();

		}
	}
	
	@Test
	public void Verify_Receipt_Group_Breakdown_Report_Run_47() throws InterruptedException, IOException {
		LoginToeGoV b=new LoginToeGoV();
		b.Verfiy_eGove_login();
		//@@
		Receipt_Group_Breakdown_Report_47 n=new Receipt_Group_Breakdown_Report_47();
		n.Verify_Receipt_Group_Breakdown_Report_47("10/05/2021", "11/12/2021");
		PDFreader pd=new PDFreader();
		pd.ReadingPDF("Receipt Breakdown By Group");
		
		
		
		

	}
	
}
