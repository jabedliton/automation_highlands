package Module.Receipts.AccountingReport;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.AccountsReports.Receipts_BalancesReportRun_46;
import TestBase.eTestBase;

public class ReceiptsBalancesReportRun_46 extends eTestBase {
	
	
		@BeforeMethod
		public void setUp() throws IOException {
			if (driver != null) {
				driver.close();

			}
		}
			
@Test	
public void Verify_RunningReceiptsBalanceReport_46() throws InterruptedException, IOException {
	LoginToeGoV b=new LoginToeGoV();
	b.Verfiy_eGove_login();
	Receipts_BalancesReportRun_46 nn=new Receipts_BalancesReportRun_46();
	nn.ReceiptsBalanceReportRunning("09/10/2021", "11/10/2021","");
	
}
}