package Module_Property_Tax.Administration;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Property_Tax.Administration.Tax_Authority_Maintenance_New_ACH_51;
import Module.Property_Tax.Administration.Tax_Authority_Maintenance_New_CHECK_49;
import TestBase.eTestBase;

public class PPT_Tax_Authority_Maintenance_New_ACH_51 extends eTestBase{

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}

	
	@Test
	public void Verify_PPT_Tax_Authority_Maintenance_New_ACH_51() throws InterruptedException, IOException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		
		Tax_Authority_Maintenance_New_ACH_51 ab=new Tax_Authority_Maintenance_New_ACH_51();
		ab.Tax_Aut_Maintenance_New_ACH("ACH","SINGLE DEPOSIT","2000200","1234567890","COUNTY - TOT MILL","102");
		
	}
	@AfterMethod
	public void teardown() {
		//driver.close();
		
	}
}
