package Module_Property_Tax.Administration;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseTestCases.LoginToeGoV;
import Module.Property_Tax.Administration.Tax_Authority_Maintenance_New_CHECK_49;
import Module.Property_Tax.Administration.Tax_Authority_Maintenance_New_Report_50;
import TestBase.eTestBase;

public class PPT_Tax_Authority_Maintenance_New_Report_50 extends eTestBase{

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}

	
	@Test
	public void Verify_PPT_Tax_Authority_Maintenance_New_Runs_50() throws InterruptedException, IOException {
		LoginToeGoV lg=new LoginToeGoV();
		lg.Verfiy_eGove_login();
		Tax_Authority_Maintenance_New_Report_50 TR=new Tax_Authority_Maintenance_New_Report_50();
		TR.Tax_Aut_Maintenance_New_Report();
		
		
		
	}
	
}
