package Module_Property_Tax.Bill;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import BaseTestCases.LoginToeGoV;
import Module.Property_Tax.Bill.CreateBillPPT;
import TestBase.eTestBase;

public class CreateBill extends eTestBase {

	
	//property tax hover over //click on Create bill// enter infos // click on save  
	
	
	CreateBillPPT CB;
	LogInPage LP;
	public int num;
	public String tc_name;
	public String eGov_Site_Url;

	public CreateBill() throws IOException, InterruptedException {

	}
//		LoginToeGoV lg = new LoginToeGoV();
//		lg.Verfiy_eGove_login();
//		// super();

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}

	@Test
	public void Verify_CreatingBill() throws InterruptedException, IOException {
		LoginToeGoV lg = new LoginToeGoV();
		lg.Verfiy_eGove_login();
		// @@@Test Data read from Excel
		tc_name = new Object() {
		}.getClass().getEnclosingMethod().getName();
		CB = new CreateBillPPT();

		// @@main
		CB.CreatingBill();
		Thread.sleep(10000);

	}

	@AfterMethod
	public void teardown() {

	}
	
   }
