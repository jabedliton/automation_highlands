package Module_Property_Tax.Bill;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import BaseTestCases.LoginToeGoV;
import Module.Property_Tax.Bill.SearchNewBill;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class SearchBillNew extends eTestBase {

	SearchNewBill Bi;
	LogInPage LP;
	public int num;
	public String tc_name;
	public String eGov_Site_Url;

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}

	@Test
	public void Verify_SearchBillnumberByNewSubMenu() throws InterruptedException, IOException {
		LoginToeGoV lg = new LoginToeGoV();
		lg.Verfiy_eGove_login();
		// @@@Test Data read from Excel
		tc_name = new Object() {
		}.getClass().getEnclosingMethod().getName();
		Bi = new SearchNewBill();
//	String EnterBillNumber = String
//			.valueOf(ExcelReadTestData.TestDataRead(prop.getProperty("TestResult"), tc_name, 3));

// @@searchBill using>> Year-Bill_Equals-AmountDue-UI balance due column value check
		Bi.searchByBillNumber("2020", "=", "0", 0.0);

	}

	@AfterMethod
	public void teardown() {

	}

}
