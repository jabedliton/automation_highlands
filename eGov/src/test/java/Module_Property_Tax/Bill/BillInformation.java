package Module_Property_Tax.Bill;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import BaseTestCases.LoginToeGoV;
import Module.Property_Tax.Bill.Bill_InformationPage;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class BillInformation extends eTestBase {
	Bill_InformationPage Bi;
	LogInPage LP;
	public int num;
	public String tc_name;
	public String eGov_Site_Url;

	public BillInformation() throws IOException, InterruptedException {

	}
//		LoginToeGoV lg = new LoginToeGoV();
//		lg.Verfiy_eGove_login();
//		// super();

	@BeforeMethod
	public void setup() {
		if (driver != null) {

			driver.close();
		}
	}

	@Test
	public void Verify_PTax_Bi_SearchByBillNumber() throws InterruptedException, IOException {
		LoginToeGoV lg = new LoginToeGoV();
		lg.Verfiy_eGove_login();
		// @@@Test Data read from Excel
		tc_name = new Object() {
		}.getClass().getEnclosingMethod().getName();
		Bi = new Bill_InformationPage();
		String EnterBillNumber = String
				.valueOf(ExcelReadTestData.TestDataRead(prop.getProperty("TestResult"), tc_name, 3));

		// @@main
		Bi.searchByBillNumber(String.valueOf(EnterBillNumber));

	}

	@AfterMethod
	public void teardown() {

	}
}
