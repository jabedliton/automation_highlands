package BaseTestCases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import BasePages.LogInPage;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class LoginToeGoV extends eTestBase {
	LogInPage LP;
	public String tc_name;
	public static String eGov_Site_Url;

	public LoginToeGoV() throws IOException {

	}

	@BeforeSuite
	public void CreateInsTC() throws InterruptedException, IOException {

	}
	@BeforeMethod
	public void setUp() throws InterruptedException, IOException {
		if (driver != null) {

			driver.close();
		}
		
	}
	   
	@Test
	public void Verfiy_eGove_login() throws InterruptedException, IOException {

		// @@@ Updating excel with new URL
		tc_name = new Object() {
		}.getClass().getEnclosingMethod().getName();
		ExcelReadTestData.Testdata_String("TestResult", tc_name,3);
		
		
		eGov_Site_Url = ExcelReadTestData.Url;
        log.info("Url is collected and passing it to initialization Method");
		// @@launching eGoV
		initialization(eGov_Site_Url);
		LP = new LogInPage();

		LP.Login(prop.getProperty("Login_Un"), prop.getProperty("Login_Pw"));

//		LP.ClickOnProfile.click();
//		LP.ClickOnLogOut.click();
//		eTestBase.driver.switchTo().alert().accept();

	}

	@AfterMethod
	public void teardown() {

	}

}
