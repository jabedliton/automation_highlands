package BaseTestCases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BasePages.DataconstantClass;
import BasePages.InstanceCreationPages;
import TestBase.eTestBase;
import UtilPackage.ExcelReadTestData;

public class Creating_Instance extends eTestBase {
	public String BaseUrl;
	InstanceCreationPages ins;
	public String insTest;

	@BeforeMethod
	public void setUp() throws IOException {
		if (driver != null) {
			driver.close();

		}
	}

	@Test
	public void Verify_InstanceCreation() throws InterruptedException, IOException {
		initialization(prop.getProperty("PHP_Con_Login"));
		ins = new InstanceCreationPages();
		BaseUrl = ins.CreateIns();
		log.info("Test url is collected=" + BaseUrl);
		ExcelReadTestData.UrlCollection(DataconstantClass.excellocation, prop.getProperty("TestResult"), 3, BaseUrl);
		// driver.close();

	}

	@AfterMethod
	public void teardown() throws IOException {

	}
}
